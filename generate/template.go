package generate

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	"gitlab.com/akabio/iotool/articache"
	"golang.org/x/tools/imports"
)

var giCache = func() articache.Cache {
	hd, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	c, err := articache.Open(filepath.Join(hd, ".cache", "stowage"))
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return c
}()

// goImports takes the source and formats it and updates imports
func goImports(file, source string) (string, error) {
	if giCache != nil {
		res, found, err := giCache.Get([]byte(source))
		if found && err == nil {
			return string(res), nil
		}
	}

	result, err := imports.Process(file, []byte(source), &imports.Options{
		Comments: true,
	})
	if err != nil {
		return "//  goimports failed, there are errors!\n" + source, err
	}
	if giCache != nil {
		err = giCache.Put([]byte(source), result)
		if err != nil {
			log.Println(err)
		}
	}
	return string(result), nil
}
