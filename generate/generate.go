package generate

import (
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/akabio/gogen"
	ggast "gitlab.com/akabio/gogen/ast"
	"gitlab.com/akabio/iotool"
	"gitlab.com/akabio/stowage/ast"
)

var (
	tplCache = map[string]*ggast.AST{}
	lock     sync.Mutex
)

func getAST(dir, name string) (*ggast.AST, error) {
	path := filepath.Join(dir, name)

	lock.Lock()
	if gg, has := tplCache[path]; has {
		lock.Unlock()
		return gg, nil
	}
	lock.Unlock()

	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	sourceFolder := filepath.Dir(path)

	loadLib := func(name string) (string, string, error) {
		fp := filepath.Join(dir, "common", name+".gg")
		d, err := os.ReadFile(fp)
		if err != nil {
			fp = filepath.Join(sourceFolder, name+".gg")
			d, err = os.ReadFile(fp)
			if err != nil {
				return "", "", err
			}
		}
		return string(d), fp, nil
	}

	gg, err := gogen.Parse(string(data), name, loadLib)
	if err != nil {
		return nil, err
	}
	lock.Lock()
	tplCache[path] = gg
	lock.Unlock()
	return gg, err
}

func Generate(ast *ast.AST, destFile, template string, vars map[string]interface{}, filters []gogen.Option) error {
	dir := iotool.MustGetSourceDir()

	gg, err := getAST(dir, template)
	if err != nil {
		return err
	}

	opts := append([]gogen.Option{}, filters...)
	for n, v := range vars {
		opts = append(opts, gogen.WithVar(n, v))
	}
	result, err := gogen.Execute(gg, ast, opts...)
	if err != nil {
		panic(err)
	}

	if strings.HasSuffix(destFile, ".go") {
		result, err = goImports(destFile, result)
		if err != nil {
			return err
		}
	}
	return iotool.WriteFileIfChanged(destFile, []byte(result), 0o644)
}
