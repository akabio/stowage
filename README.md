# STOWAGE

Generates a db access layer from a entity declaration source file.

## Declaration

### Entities

A declaration defines entities with their attributes and the relations between them.

    entity document {
      title string
    }

This example will implicitely add an ID attribute of type serial. It can also explicitely be defined like:

    entity document {
      id serial
      title string
    }

Or a custom id can be defined like:

    entity document identified by domain, id {
      id serial
      domain string
      title string
    }

### IDs

IDs are generated for all entities, either a default serial field or a composite id.

For each entity type an id Type will be generated with the given id field/fields.

### Select extensions

To extend the generated Select helpers add an inmterface Named EntityExtension to the package the interfaces are generated into. This will be included in the generated interface and allows manual extensions of the generated code.

### Views

    view documentIndex {
      id         int
      title      string
      authorName string
    }

Views will generate all the query code like for entities but no modifications.

Currently the sql will not be generated and has to be added manually for each used db backend:

    generator.Postgres("store.go",
        stowage.OptionPostCreateSQL(`
            CREATE VIEW document_index
            AS SELECT
                d.id AS id,
                LOWER(d.title) AS title,
                LOWER(CONCAT(a.first_name, ' ', a.last_name)) AS author_name
            FROM document AS d, db_user AS a
            WHERE a.id = d.author_id;
        `),
        stowage.OptionPreDropSQL(`
            DROP VIEW IF EXISTS document_index;
        `)
    )


### Attribute types

#### serial

An auto incrementing integer type, usually used as a primary key. SOme database systems one support one per entity.

#### string

A text with at least 64k bytes. If it's used as part of an id it might need to be much shorter dependening on the database.

#### int

integer type, translates to go int which is signed 32 or 64 bit depending on architecture but is signed 32 bit in the rdbms.

#### float

double precision float type

#### bool

boolean type

#### enum

    type enum (default = "", howto, help, topic = "topix")

Enum can be one of the provided values. The option `= "..."` part will overwrite the database value if it's different from the Identifier.

#### time

type with a date and time, will always be converted to and retrieved as UTC

#### reference

    author -> user

Will add a relation from the entity to the user entity.

If the target entity has a composite key it will add all key fields to the source entity.

#### unique modifier

    counter unique int

Will define a unique int field. In addition to the constraint it also generates an access method by that field.

## Code generation

Generation is best done using a `main.go` file where the configuration happens. This will add the used version and the dependencies the generated code needs to the versioning of the go toolchain.

If you add a `//go:generate go run ./` directive to the main file it will be run when you execute `go generate ./...`.

    declaration := "entity foo { ..."

    // generates golang access interfaces and datatypes in store pkg
    gen, err := stowage.Generate(declaration, "store")
    ...

    // generates postgres sql and access implementation in postgres pkg
    err = gen.Postgres("postgres")
    ...

    // generates cache which is valid for a transaction, implemented as a wrapper for a backend store
    err = gen.TransactionCache("cache")
    ...


    // generates plantuml db diagram in current folder
    err = gen.PlantUML("./")
    ...

The generated codes provides an interface to access the database. All access
goes trough transactions like:

    err = store.Do("description", func(o store.Operation) error {
      // db access
      return nil
    }

If inside the transaction function a panic is triggered or an error is returned
the transaction is aborted.

The transaction instance provides methods to create/get/update entities.

    id, err := o.CreateDocument(store.Document{Title: "first"})
    ...
    doc, err := o.GetDocument(id)
    ...
    doc.Title = "last"
    err := o.UpdateDocument(doc)

To filter entities a select object is available:

    docs, err := o.SelectDocument().TitleLike("%t").WithAuthor(johnID).Get()

The select object also provides methods to only retrieve ids, to count documents or to delete documents.
