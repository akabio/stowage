package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/example/postgres"
	"gitlab.com/akabio/stowage/test/example/store"
	"gitlab.com/akabio/stowage/test/example/test"
)

func TestGet(t *testing.T) {
	ts := nt(t)

	ts.MustDo("create all", func(o *test.TO) {
		o.MustCreateUser(store.User{
			ID: store.UserID{Login: "foo"},
		})

		o.ExpectGetUser(store.UserID{Login: "foo"}).
			ToBe(&store.User{ID: store.UserID{Login: "foo"}})
	})
}

func TestCreate(t *testing.T) {
	ts := nt(t)

	ts.MustDo("create all", func(o *test.TO) {
		o.MustCreateUser(store.User{
			ID: store.UserID{Login: "foo"},
		})

		o.MustCreateUser(store.User{
			ID: store.UserID{Login: "bar"},
		})

		docID := o.MustCreateDocument(store.Document{
			Title:  "Mamoth",
			Author: store.UserID{Login: "foo"},
		})
		commentID := o.MustCreateComment(store.Comment{
			ID:     store.CommentID{Document: docID},
			Author: store.UserID{Login: "bar"},
			State:  store.StateAccepted,
		})
		o.MustCreateVote(store.Vote{
			ID: store.VoteID{
				Comment: commentID,
				User:    store.UserID{Login: "foo"},
			},
			State: store.StateReview,
		})
	})
}

func TestSelectAll(t *testing.T) {
	ts := nt(t)

	u1 := store.User{ID: store.UserID{Login: "foo"}}
	u2 := store.User{ID: store.UserID{Login: "bar"}}
	u3 := store.User{ID: store.UserID{Login: "baz"}}

	ts.MustDo("select all", func(o *test.TO) {
		o.MustCreateUser(u1)
		o.MustCreateUser(u2)
		o.MustCreateUser(u3)

		o.SelectUser().ExpectGet().ToBe([]*store.User{&u1, &u2, &u3})
	})
}

func TestSelectByReference(t *testing.T) {
	ts := nt(t)

	ts.MustDo("select all", func(o *test.TO) {
		u1 := o.MustCreateUser(store.User{ID: store.UserID{Login: "foo"}})
		u2 := o.MustCreateUser(store.User{ID: store.UserID{Login: "bar"}})

		doc1 := o.MustCreateDocument(store.Document{Title: "d1", Author: u1})
		o.MustCreateDocument(store.Document{Title: "d2", Author: u2})
		doc3 := o.MustCreateDocument(store.Document{Title: "d3", Author: u1})

		o.SelectDocument().WithAuthor(u1).ExpectGet().ToBe([]*store.Document{
			{Title: "d1", Author: u1, ID: doc1},
			{Title: "d3", Author: u1, ID: doc3},
		})
	})
}

func nt(t *testing.T) *test.TestStore {
	pg, err := postgres.New("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
	if err != nil {
		t.Fatal(err)
	}

	return test.New(t, pg)
}
