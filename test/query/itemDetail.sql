SELECT
  item.name AS name, count(item.id) AS aspects
FROM
  item, item_aspect
WHERE
  item.id = item_aspect.item_id
GROUP BY
  item.id, item.name
;