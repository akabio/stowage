package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/query/store"
	"gitlab.com/akabio/stowage/test/query/test"
)

func TestOrderByReference(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id1 := o.MustCreateItem(store.Item{Name: "foo"})
		id2 := o.MustCreateItem(store.Item{Name: "bar"})
		id3 := o.MustCreateItem(store.Item{Name: "baz"})

		asp1 := o.MustCreateAspect(store.Aspect{Description: "x"})
		asp2 := o.MustCreateAspect(store.Aspect{Description: "y"})
		asp3 := o.MustCreateAspect(store.Aspect{Description: "z"})

		link := func(item store.ItemID, aspect store.AspectID) {
			o.MustCreateItemAspect(store.ItemAspect{ID: store.ItemAspectID{Item: item, Aspect: aspect}})
		}

		link(id1, asp2)
		link(id1, asp3)
		link(id2, asp1)
		link(id2, asp3)
		link(id3, asp2)

		o.SelectItemDetail().OrderAspectsDesc().OrderNameAsc().ExpectGet().ToBe([]*store.ItemDetail{
			{
				Aspects: 2,
				Name:    "bar",
			},
			{
				Aspects: 2,
				Name:    "foo",
			},
			{
				Aspects: 1,
				Name:    "baz",
			},
		})
	})
}
