package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestSelectView(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		u := o.MustCreateUser(store.User{FirstName: "Peter", LastName: "Meier"})
		d := o.MustCreateDocument(store.Document{
			Title:  "First Document",
			Author: u,
		})
		o.MustCreateComment(store.Comment{
			Author: u,
			ID: store.CommentID{
				Document: d,
			},
		})

		o.SelectCommentIndex().ExpectGet().ToBe([]*store.CommentIndex{{
			ID:            1,
			Document:      d,
			Author:        u,
			DocumentTitle: "first document",
			AuthorName:    "peter meier",
			Type:          "rant",
		}})

		o.SelectCommentIndex().AuthorNameLike("%pet%").ExpectGet().ToBe([]*store.CommentIndex{{
			ID:            1,
			Document:      d,
			Author:        u,
			DocumentTitle: "first document",
			AuthorName:    "peter meier",
			Type:          "rant",
		}})

		o.SelectCommentIndex().WithAuthor(u).ExpectGet().ToBe([]*store.CommentIndex{{
			ID:            1,
			Document:      d,
			Author:        u,
			DocumentTitle: "first document",
			AuthorName:    "peter meier",
			Type:          "rant",
		}})
	})
}
