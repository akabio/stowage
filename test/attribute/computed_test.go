package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestComputed(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id := o.MustCreateComp(store.Comp{
			Left:  "Aaa",
			Right: "Bbb",
		})

		o.SelectComp().IdxIs("aaa BBB").ExpectGetIDs().ToBe([]store.CompID{
			id,
		})

		o.SelectComp().IdxIs("aaa BBB").ExpectGet()
	})
}
