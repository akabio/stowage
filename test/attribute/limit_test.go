package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestLimit(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		e1 := mkEntity(1)
		e2 := mkEntity(2)
		e3 := mkEntity(3)
		e4 := mkEntity(4)
		e5 := mkEntity(5)
		e6 := mkEntity(6)
		o.MustImportEntities(e1, e2, e3, e4, e5, e6)

		o.SelectEntity().ExpectCount().ToBe(6)
		o.SelectEntity().OrderNameAsc().Limit(0, 1).ExpectGetIDs().ToBe([]store.EntityID{e1.ID})
		o.SelectEntity().OrderNameAsc().Limit(1, 5).ExpectGetIDs().ToBe([]store.EntityID{e2.ID, e3.ID, e4.ID, e5.ID})
		o.SelectEntity().OrderNameAsc().Limit(4, 5).ExpectGetIDs().ToBe([]store.EntityID{e5.ID})
		o.SelectEntity().OrderNameAsc().Limit(5, 100).ExpectGetIDs().ToBe([]store.EntityID{e6.ID})
	})
}

func TestLimitParamErrors(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.SelectEntity().OrderNameAsc().Limit(-1, 10).ExpectGetErrorMessage().ToBe("from value must be >= 0 but is -1")
		o.SelectEntity().OrderNameAsc().Limit(20, 15).ExpectGetErrorMessage().ToBe("to value must be > from but it is 15 which is less than 20")
		o.SelectEntity().OrderNameAsc().Limit(20, 20).ExpectGetErrorMessage().ToBe("to value must be > from but it is 20 which is the same as from")
	})
}
