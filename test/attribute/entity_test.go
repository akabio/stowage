package main

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestSelectEmptyResult(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.SelectEntity().ExpectGet().ToBe([]*store.Entity{})
	})
}

func TestGetMissingEntity(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.ExpectGetEntityError(store.EntityID{ID: 4}).ToBeType(&store.NotFoundError{})
		o.ExpectGetEntityError(store.EntityID{ID: 4}).Message().ToBe("entity Entity not found by condition id = (4)")

		_, err := o.Operation.GetEntity(store.EntityID{ID: 4})
		expect.Value(t, "err is not found", store.IsNotFound(err)).ToBe(true)
	})
}

func TestMissingEntityReturnsError(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.ExpectGetEntityErrorMessage(store.EntityID{ID: 4}).ToBe("entity Entity not found by condition id = (4)")
	})
}

func TestImportEntity(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo2002)
		o.ExpectGetEntity(foo2002.ID).ToBe(&foo2002)
	})
}

func TestImportEntities(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo2002, foo2003)
		o.ExpectGetEntity(foo2002.ID).ToBe(&foo2002)
		o.ExpectGetEntity(foo2003.ID).ToBe(&foo2003)
		o.SelectEntity().ExpectGet().ToCount(2)
	})
}

func TestCreateEntities(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustCreateEntities(store.Entity{Name: "A"}, store.Entity{Name: "B"}, store.Entity{Name: "C"})
		o.SelectEntity().ExpectGet().ToCount(3)
		o.SelectEntity().ExpectGet().ToBe([]*store.Entity{
			{ID: store.EntityID{ID: 1}, Name: "A"},
			{ID: store.EntityID{ID: 2}, Name: "B"},
			{ID: store.EntityID{ID: 3}, Name: "C"},
		})
	})
}

func TestImportSetsAutoID(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		o.ExpectGetEntity(foo1.ID).ToBe(&foo1)
		o2 := o.MustCreateEntity(store.Entity{Name: "bar"})
		expect.Value(t, "auto id", o2.ID).ToBe(foo1.ID.ID + 1)
	})
}

func TestGetByID(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		o.ExpectGetEntity(foo1.ID).
			ToBe(&foo1)
	})
}

func TestGetByIDs(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1, foo2002, foo2003)

		o.SelectEntity().ByIDs(foo1.ID, foo2002.ID).ExpectGet().ToBe([]*store.Entity{&foo1, &foo2002})
		o.SelectEntity().ByIDs(foo1.ID, foo1.ID).ExpectGet().ToBe([]*store.Entity{&foo1})
		o.SelectEntity().ByIDs(foo2002.ID, foo2003.ID).ExpectGet().ToBe([]*store.Entity{&foo2002, &foo2003})
		o.SelectEntity().ByIDs().ExpectGet().ToBe([]*store.Entity{})
	})
}

func TestDeleteENtityByQuery(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustCreateEntity(store.Entity{Name: "foo"})
		n8 := o.MustCreateEntity(store.Entity{Name: "bar"})

		o.SelectEntity().NameIs("foo").MustDelete()
		o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{n8})

		o.SelectEntity().MustDelete()
		o.SelectEntity().ExpectGet().ToCount(0)
	})
}

func TestDeleteEntityByQueryWithRefs(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		u1 := o.MustCreateUser(store.User{Login: "foo"})
		u2 := o.MustCreateUser(store.User{Login: "bar"})
		u3 := o.MustCreateUser(store.User{Login: "baz"})

		o.MustCreateDocument(store.Document{Author: u1})
		o.MustCreateDocument(store.Document{Author: u1})
		o.MustCreateDocument(store.Document{Author: u2})
		o.MustCreateDocument(store.Document{Author: u3, Title: "live!"})

		o.SelectDocument().WithAuthors(u1, u2).MustDelete()
		o.SelectDocument().ExpectGet().ToBe([]*store.Document{{ID: store.DocumentID{ID: 4}, Title: "live!", Author: u3}})
	})
}

func TestDeleteByID(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		n7 := o.MustCreateEntity(store.Entity{Name: "a"})
		n8 := o.MustCreateEntity(store.Entity{Name: "b"})

		o.MustDeleteEntity(n8)
		o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{n7})
		o.MustDeleteEntity(n7)
		o.SelectEntity().ExpectGetIDs().ToBe([]store.EntityID{})
	})
}

func TestSelectCount(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(mkEntity(1),
			mkEntity(2),
			mkEntity(3),
			mkEntity(4),
			mkEntity(5),
			mkEntity(6))
		o6 := mkEntity(6)
		o6.ID.ID = 7
		o.MustImportEntities(o6)

		o.SelectEntity().ExpectCount().ToBe(7)
		o.SelectEntity().NameIs("foo6").ExpectCount().ToBe(2)
	})
}

func mkEntity(id int) store.Entity {
	return store.Entity{
		ID:   store.EntityID{ID: id},
		Name: fmt.Sprintf("foo%v", id),
	}
}

var foo2002 = store.Entity{
	ID:   store.EntityID{ID: 2002},
	Name: "foo2002",
}

var foo2003 = store.Entity{
	ID:   store.EntityID{ID: 2003},
	Name: "foo2003",
}

var foo1 = store.Entity{
	ID:   store.EntityID{ID: 12},
	Name: "foo",
}
