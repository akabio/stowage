package main

import (
	"testing"
	"time"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestOrderByReference(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id1 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "baz", Year: 1}})
		id2 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "foo", Year: 1}})
		id3 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "bar", Year: 2}})

		t1 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id1}})
		t2 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id2}})
		t3 := o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id3}})

		o.SelectCompositeTag().OrderCompositeNameDesc().ExpectGetIDs().ToBe([]store.CompositeTagID{t2, t1, t3})
	})
}

func TestOrderBySerial(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustCreateEntity(store.Entity{Name: "a"})
		o.MustCreateEntity(store.Entity{Name: "b"})
		o.MustCreateEntity(store.Entity{Name: "c"})
		o.SelectEntity().OrderIDDesc().ExpectGetIDs().ToBe([]store.EntityID{{ID: 3}, {ID: 2}, {ID: 1}})
	})
}

func TestOrderByDate(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := time.Unix(123456789, 345678000)
		t2 := t1.Add(time.Second)
		t3 := t1.Add(time.Second * 1000)
		id2 := o.MustCreatePrimitive(store.Primitive{Time: t2})
		id1 := o.MustCreatePrimitive(store.Primitive{Time: t1})
		id3 := o.MustCreatePrimitive(store.Primitive{Time: t3})

		o.SelectPrimitive().OrderTimeAsc().ExpectGetIDs().ToBe([]store.PrimitiveID{id1, id2, id3})
		o.SelectPrimitive().OrderTimeDesc().ExpectGetIDs().ToBe([]store.PrimitiveID{id3, id2, id1})
	})
}

func TestOrderByJoinedField(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		userFoo := o.MustCreateUser(store.User{Login: "foo"})
		userBar := o.MustCreateUser(store.User{Login: "bar"})

		doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
		doc2 := o.MustCreateDocument(store.Document{Author: userBar})

		o.SelectDocument().JoinAuthor(
			o.SelectUser().OrderLoginAsc(),
		).ExpectGetIDs().ToBe([]store.DocumentID{doc2, doc1})

		o.SelectDocument().JoinAuthor(
			o.SelectUser().OrderLoginDesc(),
		).ExpectGetIDs().ToBe([]store.DocumentID{doc1, doc2})
	})
}
