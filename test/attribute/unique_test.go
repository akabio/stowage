package main

import (
	"testing"
	"time"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestUniqueSelect(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		un := store.Unique{
			Float:  17.04,
			Int:    128,
			String: "foo",
			Time:   time.Unix(123456789, 345678000).UTC(),
		}

		id := o.MustCreateUnique(un)
		un.ID = id

		o.MustCreateUnique(store.Unique{
			Float:  17.0467,
			Int:    129,
			String: "bar",
			Time:   time.Unix(123456789, 345679000).UTC(),
		})

		o.ExpectGetUniqueByFloat(17.04).ToBe(&un)
		o.ExpectGetUniqueByString("foo").ToBe(&un)
		o.ExpectGetUniqueByInt(128).ToBe(&un)
		o.ExpectGetUniqueByTime(time.Unix(123456789, 345678000).UTC()).ToBe(&un)
	})
}
