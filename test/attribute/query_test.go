package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestOrQuery(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		for _, n := range []string{"foo", "peter", "pan", "bar"} {
			o.MustCreateEntity(store.Entity{
				Name: n,
			})
		}

		o.SelectEntity().Or(func(q store.EntityQuery) {
			q.NameIs("peter").NameIs("pan")
		}).ExpectGet().ToBe([]*store.Entity{
			{ID: store.EntityID{ID: 2}, Name: "peter"},
			{ID: store.EntityID{ID: 3}, Name: "pan"},
		})
	})
}

func TestInQuery(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		for _, n := range []string{"foo", "peter", "pan", "bar", "baz"} {
			o.MustCreateEntity(store.Entity{
				Name: n,
			})
		}

		o.SelectEntity().NameIn("pan", "baz").ExpectGet().ToBe([]*store.Entity{
			{ID: store.EntityID{ID: 3}, Name: "pan"},
			{ID: store.EntityID{ID: 5}, Name: "baz"},
		})
	})
}
