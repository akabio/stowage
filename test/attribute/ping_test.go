package main

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestPingDB(t *testing.T) {
	run(t, func(t *testing.T, ts *test.TestStore, tt tType) {
		expect.Value(t, "ping response", ts.Store.Ping()).ToBe(nil)
	})
}
