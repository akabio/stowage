package main

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestQueryByIsAttribute(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		ttt := time.Unix(123456789, 345678000)
		fmt.Println(ttt)
		id := o.MustCreatePrimitive(store.Primitive{
			Enum:   store.PreFoo,
			Float:  17.0455665,
			Int:    -124,
			Bool:   true,
			String: "FOO",
			Time:   time.Unix(123456789, 345678000),
		})
		o.MustCreatePrimitive(store.Primitive{
			Enum: store.PreDefault,
		})

		o.SelectPrimitive().IntIs(-124).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().BoolIs(true).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().StringIs("FOO").ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().EnumIs(store.PreFoo).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().FloatIs(17.0455665).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().TimeIs(time.Unix(123456789, 345678000)).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
	})
}

func TestQueryByDateComparison(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		tm := time.Unix(123456789, 345678000)
		after := tm.Add(time.Second)
		before := tm.Add(-time.Second)
		id := o.MustCreatePrimitive(store.Primitive{
			Time: time.Unix(123456789, 345678000),
		})

		o.SelectPrimitive().TimeIsGreaterEqual(tm).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().TimeIsGreaterEqual(after).ExpectGetIDs().ToBe([]store.PrimitiveID{})

		o.SelectPrimitive().TimeIsSmallerEqual(tm).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
		o.SelectPrimitive().TimeIsSmallerEqual(before).ExpectGetIDs().ToBe([]store.PrimitiveID{})

		o.SelectPrimitive().TimeIsGreater(tm).ExpectGetIDs().ToBe([]store.PrimitiveID{})
		o.SelectPrimitive().TimeIsGreater(before).ExpectGetIDs().ToBe([]store.PrimitiveID{id})

		o.SelectPrimitive().TimeIsSmaller(tm).ExpectGetIDs().ToBe([]store.PrimitiveID{})
		o.SelectPrimitive().TimeIsSmaller(after).ExpectGetIDs().ToBe([]store.PrimitiveID{id})
	})
}

func TestCreateAndGetPrimitives(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id := o.MustCreatePrimitive(store.Primitive{
			ID:     store.PrimitiveID{},
			Enum:   store.PreDefault,
			Float:  17.0455665,
			Int:    -124,
			Bool:   true,
			String: "fooo-bar\nbaaar",
			Time:   time.Unix(123456789, 345678000),
		})

		o.ExpectGetPrimitive(id).ToBe(&store.Primitive{
			ID:     id,
			Enum:   store.PreDefault,
			Float:  17.0455665,
			Int:    -124,
			Bool:   true,
			String: "fooo-bar\nbaaar",
			Time:   time.Unix(123456789, 345678000).UTC(),
		})
	})
}

func TestUpdatePrimitives(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		prim := store.Primitive{
			Enum:   store.PreDefault,
			Float:  17.0455665,
			Int:    -124,
			Bool:   true,
			String: "fooo-bar\nbaaar",
			Time:   time.Unix(123456789, 345678000).UTC(),
		}

		prim.ID = o.MustCreatePrimitive(store.Primitive{Enum: store.PreDefault})

		o.MustUpdatePrimitive(prim)

		o.ExpectGetPrimitive(prim.ID).ToBe(&prim)
	})
}

func TestQueryByLike(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		f := o.MustCreatePrimitive(store.Primitive{String: "FOO"})
		b := o.MustCreatePrimitive(store.Primitive{String: "BAR"})
		z := o.MustCreatePrimitive(store.Primitive{String: "BAZ"})
		fbz := o.MustCreatePrimitive(store.Primitive{String: "FOOBARBAZ"})

		o.SelectPrimitive().StringLike("FOO%").ExpectGetIDs().ToBe([]store.PrimitiveID{f, fbz})
		o.SelectPrimitive().StringLike("BA_").ExpectGetIDs().ToBe([]store.PrimitiveID{b, z})
		o.SelectPrimitive().StringLike("%BAR%").ExpectGetIDs().ToBe([]store.PrimitiveID{b, fbz})
	})
}

func TestInvalidEnumName(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == Postgres {
			o.ExpectCreatePrimitiveError(store.Primitive{Enum: store.Pre("nope")}).Message().
				ToHavePrefix("invalid value 'nope' for enum pre")
		}
	}, errorIsOK)

	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == MSSQL {
			o.ExpectCreatePrimitiveError(store.Primitive{Enum: store.Pre("nope")}).Message().
				ToHavePrefix("invalid value 'nope' for enum primitiveEnum")
		}
	}, errorIsOK)

	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == MySQL {
			o.ExpectCreatePrimitiveError(store.Primitive{Enum: store.Pre("nope")}).Message().
				ToHavePrefix("Error 1265 (01000): Data truncated for column 'enum' at row 1")
		}
	}, errorIsOK)
}
