package main

import (
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestInvalidReferenceError(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == Postgres {
			o.ExpectCreateBranchError(store.Branch{}).Message().ToHavePrefix("branch.stem must reference an existing trunk instance (0)")
		}
	}, errorIsOK)

	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == MySQL || tt == MSSQL {
			o.ExpectCreateBranchError(store.Branch{}).Message().ToHavePrefix("branch.stem must reference an existing trunk instance")
		}
	}, errorIsOK)
}

func TestInvalidMultikeyReferenceError(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == Postgres {
			o.ExpectCreateCompositeTagError(store.CompositeTag{ID: store.CompositeTagID{Composite: store.CompositeID{Name: "snake", Year: 2000}}}).
				Message().ToHavePrefix("composite_tag.composite must reference an existing composite instance (snake, 2000)")
		}
	}, errorIsOK)

	st(t, func(t *testing.T, o *test.TO, tt tType) {
		if tt == MySQL || tt == MSSQL {
			o.ExpectCreateCompositeTagError(store.CompositeTag{ID: store.CompositeTagID{Composite: store.CompositeID{Name: "snake", Year: 2000}}}).
				Message().ToHavePrefix("composite_tag.composite must reference an existing composite instance")
		}
	}, errorIsOK)
}
