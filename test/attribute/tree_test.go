package main

import (
	"fmt"
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestFindLeafsByTrunk(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := createTreeStruct(o, "foo", 3)
		t2 := createTreeStruct(o, "bar", 2)

		o.SelectLeaf().JoinBranch(o.SelectBranch().WithStem(t1.ID)).ExpectGet().ToCount(9)
		o.SelectLeaf().JoinBranch(o.SelectBranch().WithStem(t2.ID)).ExpectGet().ToCount(4)
	})
}

func TestFindLeafsByTrunks(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := createTreeStruct(o, "foo", 3)
		t2 := createTreeStruct(o, "bar", 2)
		t3 := createTreeStruct(o, "baz", 1)

		o.SelectLeaf().JoinBranch(o.SelectBranch().WithStems(t1.ID, t2.ID)).ExpectGet().ToCount(13)
		o.SelectLeaf().JoinBranch(o.SelectBranch().WithStems(t2.ID, t3.ID)).ExpectGet().ToCount(5)
		o.SelectLeaf().JoinBranch(o.SelectBranch().WithStems(t2.ID, t3.ID, t1.ID)).ExpectGet().ToCount(14)
	})
}

func TestSelectByWith(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := createTreeStruct(o, "foo", 3)
		t2 := createTreeStruct(o, "bar", 2)
		o.SelectBranch().WithStem(t1.ID).ExpectGet().ToCount(3)
		o.SelectBranch().WithStem(t2.ID).ExpectGet().ToCount(2)
	})
}

func TestSelectByWithMany(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := createTreeStruct(o, "foo", 3)
		t2 := createTreeStruct(o, "bar", 2)
		createTreeStruct(o, "baz", 1)
		o.SelectBranch().WithStems(t1.ID, t2.ID).ExpectGet().ToCount(5)
	})
}

func TestFindTrunkByLeaf(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		t1 := createTreeStruct(o, "foo", 3)
		t2 := createTreeStruct(o, "bar", 2)

		o.SelectTrunk().JoinBranch(o.SelectBranch().JoinLeaf(o.SelectLeaf().NameIs("foo_1_1"))).
			ExpectGet().ToBe([]*store.Trunk{&t1})
		o.SelectTrunk().JoinBranch(o.SelectBranch().JoinLeaf(o.SelectLeaf().NameIs("bar_0_1"))).
			ExpectGet().ToBe([]*store.Trunk{&t2})
	})
}

func createTreeStruct(op *test.TO, name string, n int) store.Trunk {
	t := store.Trunk{Name: name}
	t.ID = op.MustCreateTrunk(t)
	for i := 0; i < n; i++ {
		b := store.Branch{
			Stem: t.ID,
			Name: fmt.Sprintf("%v_%v", name, i),
		}
		b.ID = op.MustCreateBranch(b)
		for j := 0; j < n; j++ {
			l := store.Leaf{
				Branch: b.ID,
				Name:   fmt.Sprintf("%v_%v_%v", name, i, j),
			}
			op.MustCreateLeaf(l)
		}
	}
	return t
}
