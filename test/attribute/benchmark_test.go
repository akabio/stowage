package main

import (
	"strconv"
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/mysql"
	"gitlab.com/akabio/stowage/test/attribute/postgres"
	"gitlab.com/akabio/stowage/test/attribute/store"
)

func backends(b *testing.B) map[string]store.Store {
	b.Helper()

	var err error
	bs := map[string]store.Store{}

	bs["postgres"], err = postgres.New("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
	if err != nil {
		b.Fatal(err)
	}

	bs["mysql"], err = mysql.New("mysql:mysql@tcp(127.0.0.1)/mysql")
	if err != nil {
		b.Fatal(err)
	}

	return bs
}

func BenchmarkTransaction1Insert1000(b *testing.B) {
	for backend, store := range backends(b) {
		b.Run(backend, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				runInsert(b, store, 1, 1000)
			}
		})
	}
}

func BenchmarkTransaction100Insert10(b *testing.B) {
	for backend, store := range backends(b) {
		b.Run(backend, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				runInsert(b, store, 100, 10)
			}
		})
	}
}

func BenchmarkTransaction10Insert1000(b *testing.B) {
	for backend, store := range backends(b) {
		b.Run(backend, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				runInsert(b, store, 10, 1000)
			}
		})
	}
}

func runInsert(b *testing.B, st store.Store, t, ins int) {
	err := st.Reset()
	if err != nil {
		b.Fatal(err)
	}
	var trID store.TrunkID
	err = st.Do("benchmark init", func(o store.Operation) error {
		trID, err = o.CreateTrunk(store.Trunk{Name: "TRUNK"})
		return err
	})
	if err != nil {
		b.Fatal(err)
	}

	for ti := 0; ti < t; ti++ {
		err = st.Do("benchmark", func(o store.Operation) error {
			for i := 0; i < ins; i++ {
				_, err = o.CreateBranch(store.Branch{
					Name: "Foo" + strconv.Itoa(ti*ins+i),
					Stem: trID,
				})
			}
			return err
		})
		if err != nil {
			b.Fatal(err)
		}
	}
}
