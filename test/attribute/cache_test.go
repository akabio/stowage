package main

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestCachedItemPointsToDifferentInstance(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		fo1A := o.MustGetEntity(foo1.ID)
		fo1B := o.MustGetEntity(foo1.ID)
		fo1C := o.MustGetEntity(foo1.ID)
		fo1A.Name = "crooked"
		fo1D := o.MustGetEntity(foo1.ID)

		expect.Value(t, "cached entity name", fo1B.Name).NotToBe("crooked") // first from cache
		expect.Value(t, "cached entity name", fo1C.Name).NotToBe("crooked") // second from cache
		expect.Value(t, "cached entity name", fo1D.Name).NotToBe("crooked") // from cache after original modified
	})
}

func TestCachedItemGetsInvalidatedByUpdate(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		fi := o.MustGetEntity(foo1.ID)
		fi.Name = "crooked"
		o.MustUpdateEntity(*fi)
		cached := o.MustGetEntity(foo1.ID)
		expect.Value(t, "cached entity name", cached.Name).ToBe("crooked")
	})
}

func TestCachedItemGetsInvalidatedByDelete(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		fi := o.MustGetEntity(foo1.ID)
		o.MustDeleteEntity(fi.ID)
		o.ExpectGetEntityErrorMessage(fi.ID).ToHavePrefix("entity Entity not found")
	})
}

func TestCachedItemGetsInvalidatedByDeleteBySelect(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		o.MustImportEntities(foo1)
		fi := o.MustGetEntity(foo1.ID)
		o.SelectEntity().MustDelete()
		o.ExpectGetEntityErrorMessage(fi.ID).ToHavePrefix("entity Entity not found")
	})
}
