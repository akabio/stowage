package main

import (
	"fmt"
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestSimpleJoin(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		userFoo := o.MustCreateUser(store.User{Login: "foo"})
		userBar := o.MustCreateUser(store.User{Login: "bar"})

		doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
		doc2 := o.MustCreateDocument(store.Document{Author: userBar})

		comment1 := o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc1}})
		o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc2}})

		o.SelectComment().JoinDocument(
			o.SelectDocument().WithAuthor(userFoo),
		).ExpectGet().ToBe([]*store.Comment{{ID: comment1, Author: userBar}})
	})
}

func TestN2NJoin(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		userFoo := o.MustCreateUser(store.User{Login: "foo"})
		fmt.Println(userFoo)
		userBar := o.MustCreateUser(store.User{Login: "bar"})
		fmt.Println(userBar)

		doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
		fmt.Println(doc1)
		doc2 := o.MustCreateDocument(store.Document{Author: userBar})
		fmt.Println(doc2)

		o.MustCreateRight(store.Right{
			ID:       store.RightID{User: userFoo, Document: doc1},
			Function: store.FunctionEditor,
		})
		o.MustCreateRight(store.Right{
			ID:       store.RightID{User: userFoo, Document: doc2},
			Function: store.FunctionEditor,
		})

		o.MustCreateRight(store.Right{
			ID:       store.RightID{User: userBar, Document: doc1},
			Function: store.FunctionEditor,
		})

		o.SelectUser().JoinRight(
			o.SelectRight().FunctionIs(store.FunctionEditor).WithDocument(doc1),
		).OrderIDAsc().ExpectGetIDs().ToBe([]store.UserID{userFoo, userBar})

		o.SelectUser().JoinRight(
			o.SelectRight().FunctionIs(store.FunctionEditor).WithDocument(doc2),
		).ExpectGetIDs().ToBe([]store.UserID{userFoo})
	})
}

func TestTwoJoins(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		userFoo := o.MustCreateUser(store.User{Login: "foo"})
		userBar := o.MustCreateUser(store.User{Login: "bar"})

		doc1 := o.MustCreateDocument(store.Document{Author: userFoo})
		doc2 := o.MustCreateDocument(store.Document{Author: userBar})

		comment1 := o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc1}})
		comment2 := o.MustCreateComment(store.Comment{Author: userBar, ID: store.CommentID{Document: doc2}})
		vote1 := o.MustCreateVote(store.Vote{
			ID: store.VoteID{Comment: comment1, User: userBar},
		})
		o.MustCreateVote(store.Vote{
			ID: store.VoteID{Comment: comment1, User: userFoo},
		})
		o.MustCreateVote(store.Vote{
			ID: store.VoteID{Comment: comment2, User: userBar},
		})

		o.SelectVote().JoinUser(
			o.SelectUser().LoginIs("bar"),
		).JoinComment(
			o.SelectComment().JoinDocument(o.SelectDocument().JoinAuthor(o.SelectUser().LoginIs("foo"))),
		).
			ExpectGet().ToBe([]*store.Vote{{ID: vote1}})
	})
}
