package main

import (
	"os"
	"testing"

	"gitlab.com/akabio/stowage/test/attribute/mssql"
	"gitlab.com/akabio/stowage/test/attribute/mysql"
	"gitlab.com/akabio/stowage/test/attribute/postgres"
	"gitlab.com/akabio/stowage/test/attribute/serializer"
	"gitlab.com/akabio/stowage/test/attribute/sqlite"
	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
	"gitlab.com/akabio/stowage/test/attribute/txcache"
)

type tType int

const (
	Postgres tType = iota
	MySQL
	SQLite
	MSSQL
)

var testTarget = os.Getenv("TEST_TARGET")

// run runs the provided func once for each db as a separate test
func run(t *testing.T, f func(*testing.T, *test.TestStore, tType)) {
	runTest := func(t *testing.T, st store.Store, tt tType) {
		seri := serializer.New(st)
		txdb := txcache.NewLRU(100, seri)
		ts := test.New(t, txdb)

		f(t, ts, tt)
	}
	if is(Postgres) {
		t.Run("postgres", func(t *testing.T) {
			pdb, err := postgres.New("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
			if err != nil {
				t.Fatal(err)
			}
			runTest(t, pdb, Postgres)
		})
	}
	if is(MySQL) {
		t.Run("mysql", func(t *testing.T) {
			mdb, err := mysql.New("mysql:mysql@tcp(127.0.0.1)/mysql")
			if err != nil {
				t.Fatal(err)
			}
			runTest(t, mdb, MySQL)
		})
	}
	if is(SQLite) {
		t.Run("sqlite", func(t *testing.T) {
			mdb, err := sqlite.New(":memory:")
			if err != nil {
				t.Fatal(err)
			}
			runTest(t, mdb, SQLite)
		})
	}
	if is(MSSQL) {
		t.Run("mssql", func(t *testing.T) {
			mdb, err := mssql.New(`Server=localhost;database=test;Persist Security Info=False;User ID=sa;Password=superPassword42;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;`)
			if err != nil {
				t.Fatal(err)
			}
			runTest(t, mdb, MSSQL)
		})
	}
}

// st runs the provided func once for each db as a separate test
// inside a transaction.
func st(t *testing.T, f func(*testing.T, *test.TO, tType), efs ...func(*testing.T, error, tType)) {
	runTest := mkRunTest(f, efs...)

	t.Helper()
	// pack all the tests into a group so we can run those parallel
	// but only to the others of the same group
	t.Run("group", func(t *testing.T) {
		if is(Postgres) {
			t.Run("postgres", func(t *testing.T) {
				t.Parallel()
				pdb, err := postgres.New("host=localhost port=5432 user=postgres password=postgres dbname=postgres sslmode=disable")
				if err != nil {
					t.Fatal(err)
				}
				runTest(t, pdb, Postgres)
			})
		}
		if is(MySQL) {
			t.Run("mysql", func(t *testing.T) {
				t.Parallel()
				mdb, err := mysql.New("mysql:mysql@tcp(127.0.0.1)/mysql")
				if err != nil {
					t.Fatal(err)
				}
				runTest(t, mdb, MySQL)
			})
		}
		if is(SQLite) {
			t.Run("sqlite", func(t *testing.T) {
				t.Parallel()
				mdb, err := sqlite.New("file:memdb1?mode=memory&cache=shared")
				if err != nil {
					t.Fatal(err)
				}
				runTest(t, mdb, SQLite)
			})
		}
		if is(MSSQL) {
			t.Run("mssql", func(t *testing.T) {
				t.Parallel()
				mdb, err := mssql.New(`Server=localhost;database=test;Persist Security Info=False;User ID=sa;Password=superPassword42;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;`)
				if err != nil {
					t.Fatal(err)
				}
				runTest(t, mdb, MSSQL)
			})
		}
	})
}

func mkRunTest(f func(*testing.T, *test.TO, tType), efs ...func(*testing.T, error, tType)) func(*testing.T, store.Store, tType) {
	return func(t *testing.T, st store.Store, tt tType) {
		seri := serializer.New(st)
		txdb := txcache.NewLRU(100, seri)
		ts := test.New(t, txdb)
		err := ts.Do("", func(to *test.TO) {
			f(t, to, tt)
		})

		if len(efs) == 0 {
			// if we have no error check functions
			if err != nil {
				// we fail if there was an error
				t.Fatal(err)
			}
			// or we just return
			return
		}
		for _, ef := range efs {
			// in case there are error check functions we
			// call each one with err or nil
			ef(t, err, tt)
		}
	}
}

func is(tp tType) bool {
	if testTarget == "" {
		return true
	}
	if testTarget == "postgres" && tp == Postgres {
		return true
	}
	if testTarget == "mysql" && tp == MySQL {
		return true
	}
	if testTarget == "sqlite" && tp == SQLite {
		return true
	}
	if testTarget == "mssql" && tp == MSSQL {
		return true
	}
	return false
}

// errorIsOK as an error param will ignore transaction errors
func errorIsOK(*testing.T, error, tType) {
}
