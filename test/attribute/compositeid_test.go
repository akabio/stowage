package main

import (
	"testing"

	"github.com/akabio/expect"
	"gitlab.com/akabio/stowage/test/attribute/store"
	"gitlab.com/akabio/stowage/test/attribute/test"
)

func TestNonAutoKeyComposite(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id1 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "baz", Year: 1}})
		expect.Value(t, "pk", id1).ToBe(store.CompositeID{Name: "baz", Year: 1})
	})
}

func TestCompositeWithRef(t *testing.T) {
	// old: SELECT c.composite_name, c.composite_year, c.tag FROM composite_tag AS c WHERE ((c.composite_name = $1 AND c.composite_year = $2) OR (c.composite_name = $3 AND c.composite_year = $4))
	// new: SELECT c.composite_name, c.composite_year, c.tag FROM composite_tag AS c WHERE (c.composite_name, c.composite_year) IN (($1, $2), ($3, $4))
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id1 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "baz", Year: 1}})
		id2 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "bar", Year: 2}})
		id3 := o.MustCreateComposite(store.Composite{ID: store.CompositeID{Name: "baz", Year: 3}})

		o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id1, Tag: "a"}})
		o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id2, Tag: "a"}})
		o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id1, Tag: "b"}})
		o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id2, Tag: "c"}})
		o.MustCreateCompositeTag(store.CompositeTag{ID: store.CompositeTagID{Composite: id3, Tag: "d"}})

		o.SelectCompositeTag().WithComposites(id1, id3).ExpectGetIDs().ToBe([]store.CompositeTagID{
			{Composite: id1, Tag: "a"},
			{Composite: id1, Tag: "b"},
			{Composite: id3, Tag: "d"},
		})
	})
}

func TestCreateCompositeWithAutoKey(t *testing.T) {
	st(t, func(t *testing.T, o *test.TO, tt tType) {
		id1 := o.MustCreateCompositeWithSerial(store.CompositeWithSerial{ID: store.CompositeWithSerialID{Tag: "foo"}})
		expect.Value(t, "pk", id1.ID).NotToBe(0)
	})
}
