package main

//go:generate go run ./

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"gitlab.com/akabio/iotool"
	"gitlab.com/akabio/stowage"
)

func main() {
	start := time.Now()
	root := path.Dir(path.Dir(iotool.MustGetSourceDir()))

	files, err := ioutil.ReadDir(root)
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		if f.IsDir() {
			data, err := os.ReadFile(filepath.Join(root, f.Name(), f.Name()+".pg"))
			if os.IsNotExist(err) {
				continue
			}
			if err != nil {
				log.Fatal(err)
			}

			gen, err := stowage.Generate(data, filepath.Join(root, f.Name()), filepath.Join(root, f.Name(), "store"), stowage.OptionCustomPlural("data", "datas"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.Postgres(filepath.Join(root, f.Name(), "postgres"), optsForPostgres(f.Name())...)
			if err != nil {
				log.Fatal(err)
			}

			err = gen.MySQL(filepath.Join(root, f.Name(), "mysql"), optsForMySQL(f.Name())...)
			if err != nil {
				log.Fatal(err)
			}

			err = gen.SQLite(filepath.Join(root, f.Name(), "sqlite"), optsForSQLite(f.Name())...)
			if err != nil {
				log.Fatal(err)
			}

			err = gen.MSSQL(filepath.Join(root, f.Name(), "mssql"), optsForMSSQL(f.Name())...)
			if err != nil {
				log.Fatal(err)
			}

			err = gen.Test(filepath.Join(root, f.Name(), "test"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.TransactionCache(filepath.Join(root, f.Name(), "txcache"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.Serializer(filepath.Join(root, f.Name(), "serializer"))
			if err != nil {
				log.Fatal(err)
			}

			err = gen.PlantUML(filepath.Join(root, f.Name()), "' top comment", "' bottom comment")
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	fmt.Println(time.Since(start))
}

func optsForMSSQL(name string) []stowage.Option {
	if name == "attribute" {
		return []stowage.Option{
			stowage.OptionPostCreateSQL(`
			EXECUTE('CREATE VIEW comment_index
				AS SELECT
					c.id AS id,
					d.id AS document_id,
					a.id AS author_id,
					''rant'' AS type,
					LOWER(d.title) AS document_title,
					LOWER(CONCAT(a.first_name, '' '', a.last_name)) AS author_name
				FROM comment AS c, document AS d, db_user AS a
				WHERE d.id = c.document_id AND a.id = c.author_id');
			`),
			stowage.OptionPreDropSQL(`
				DROP VIEW IF EXISTS comment_index;
			`),
		}
	}

	return []stowage.Option{}
}

func optsForPostgres(name string) []stowage.Option {
	if name == "attribute" {
		return []stowage.Option{
			stowage.OptionPostCreateSQL(`
				CREATE VIEW comment_index
				AS SELECT
					c.id AS id,
					d.id AS document_id,
					a.id AS author_id,
					'rant' AS type,
					LOWER(d.title) AS document_title,
					LOWER(CONCAT(a.first_name, ' ', a.last_name)) AS author_name
				FROM comment AS c, document AS d, db_user AS a
				WHERE d.id = c.document_id AND a.id = c.author_id;
			`),
			stowage.OptionPreDropSQL(`
				DROP VIEW IF EXISTS comment_index;
			`),
		}
	}

	return []stowage.Option{}
}

func optsForMySQL(name string) []stowage.Option {
	if name == "attribute" {
		return []stowage.Option{
			stowage.OptionPostCreateSQL(`
				CREATE OR REPLACE VIEW comment_index
				AS SELECT
					c.id AS id,
					d.id AS document_id,
					a.id AS author_id,
					"rant" AS type,
					LOWER(d.title) AS document_title,
					LOWER(CONCAT(a.first_name, ' ', a.last_name)) AS author_name
				FROM comment AS c, document AS d, db_user AS a
				WHERE d.id = c.document_id AND a.id = c.author_id;
			`),
			stowage.OptionPreDropSQL(`
				DROP VIEW IF EXISTS comment_index;
			`),
		}
	}

	return []stowage.Option{}
}

func optsForSQLite(name string) []stowage.Option {
	if name == "attribute" {
		return []stowage.Option{
			stowage.OptionPostCreateSQL(`
				CREATE VIEW IF NOT EXISTS comment_index
				AS SELECT
					c.id AS id,
					d.id AS document_id,
					a.id AS author_id,
					"rant" AS type,
					LOWER(d.title) AS document_title,
					LOWER(a.first_name || ' ' || a.last_name) AS author_name
				FROM comment AS c, document AS d, db_user AS a
				WHERE d.id = c.document_id AND a.id = c.author_id;
			`),
			stowage.OptionPreDropSQL(`
				DROP VIEW IF EXISTS comment_index;
			`),
		}
	}

	return []stowage.Option{}
}
