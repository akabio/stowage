package ast

type EnumType struct {
	Name   string
	Values []EnumValue
}

func (e EnumType) Short() string {
	r := ""
	for i, e := range e.Values {
		if i > 0 {
			r += "/"
		}
		r += e.ID
	}
	if len(r) > 20 {
		r = r[:18] + "..."
	}
	return r
}

func (e EnumType) MaxLength() int {
	max := 1
	for _, v := range e.Values {
		if len(v.Value) > max {
			max = len(v.Value)
		}
	}
	return max
}

type EnumValue struct {
	ID    string
	Value string
}

func (e *EnumType) IsAuto() bool {
	return false
}

func (e *EnumType) String() string {
	return "enum"
}
