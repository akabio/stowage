package ast

import (
	"strings"
)

type Entity struct {
	Name     string
	Comments []string
	IsView   bool
	// Entity is a CTE or equivalent
	IsQuery bool
	// Query string when the entity is a CTE
	Query     string
	Extension bool

	// Attributes are all attributes defined on this entity including ID
	Attributes Attributes

	InverseAttributes Attributes
}

func (e *Entity) IsVirtual() bool {
	return e.IsView || e.IsQuery
}

func (e *Entity) HasSerialInCompositeIdentity() bool {
	parts := e.Attributes.OnlyIDs().Resolve()
	if len(parts) <= 1 {
		return false
	}
	for _, ra := range parts {
		// only if we have multiple primary keys and there is one serial on this entity directly we return true
		if len(ra) == 1 && ra.Type().String() == "serial" {
			return true
		}
	}
	return false
}

func (e *Entity) SerialInCompositeIdentity() ResolvedAttribute {
	parts := e.Attributes.OnlyIDs().Resolve()
	if len(parts) <= 1 {
		return ResolvedAttribute{}
	}

	for _, ra := range parts {
		if len(ra) == 1 && ra.Type().String() == "serial" {
			return ra
		}
	}
	return ResolvedAttribute{}
}

type Attribute struct {
	Name            string
	InverseName     string
	Identity        bool
	InverseIdentity bool
	Unique          bool
	AsType          Type
	Computed        Expression
}

func (a *Attribute) Type() Type {
	if a.Computed != nil {
		return a.Computed.Type()
	}

	return a.AsType
}

func (a *Attribute) Resolve() []ResolvedAttribute {
	return Attributes{a}.Resolve()
}

type Attributes []*Attribute

func (a Attributes) NoIDs() Attributes {
	return a.filter(func(att *Attribute) bool { return !att.Identity })
}

func (a Attributes) OnlyIDs() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Identity })
}

func (a Attributes) NoComputed() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Computed == nil })
}

func (a Attributes) OnlyComputed() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Computed != nil })
}

func (a Attributes) NoAuto() Attributes {
	return a.filter(func(att *Attribute) bool { return !att.Type().IsAuto() })
}

func (a Attributes) OnlyAuto() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Type().IsAuto() })
}

func (a Attributes) OnlyUnique() Attributes {
	return a.filter(func(att *Attribute) bool { return att.Unique })
}

func (a Attributes) OnlyReferences() Attributes {
	return a.filter(func(att *Attribute) bool { _, is := att.Type().(*Reference); return is })
}

func (a Attributes) NoReferences() Attributes {
	return a.filter(func(att *Attribute) bool { _, is := att.Type().(*Reference); return !is })
}

func (a Attributes) filter(f func(a *Attribute) bool) Attributes {
	atts := Attributes{}
	for _, att := range a {
		if f(att) {
			atts = append(atts, att)
		}
	}
	return atts
}

func (a Attributes) Resolve() []ResolvedAttribute {
	fats := []ResolvedAttribute{}
	for _, att := range a {
		if ref, is := att.Type().(*Reference); is {
			for _, subFats := range ref.Entity.Attributes.OnlyIDs().Resolve() {
				afats := ResolvedAttribute{att}
				afats = append(afats, subFats...)
				fats = append(fats, afats)
			}
		} else {
			fats = append(fats, ResolvedAttribute{att})
		}
	}
	return fats
}

type ResolvedAttribute []*Attribute

func (ra ResolvedAttribute) Name() string {
	s := ""

	for _, a := range ra {
		s += strings.ToUpper(a.Name[:1]) + a.Name[1:]
	}
	return s
}

func (ra ResolvedAttribute) Type() Type {
	return ra[len(ra)-1].Type()
}

func (ra ResolvedAttribute) Attribute() *Attribute {
	return ra[len(ra)-1]
}
