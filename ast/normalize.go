package ast

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

// Normalize takes the raw ast from the parser and
// updates it state so it's ready to be rendered
func Normalize(r *AST, sourceFolder string) error {
	err := resolveEntities(r)
	if err != nil {
		return err
	}

	err = loadQueries(r, sourceFolder)
	if err != nil {
		return err
	}

	enumNames(r)

	return nil
}

func loadQueries(r *AST, srcFolder string) error {
	for _, e := range r.Entities {
		if e.IsQuery {
			qd, err := os.ReadFile(filepath.Join(srcFolder, e.Name+".sql"))
			if err != nil {
				return err
			}

			e.Query = strings.TrimRight(string(qd), "; \n\t")
		}
	}

	return nil
}

// resolveEntities resolves references to entities
func resolveEntities(r *AST) error {
	for _, e := range r.Entities {
		for _, a := range e.Attributes {
			if ref, is := a.Type().(*Reference); is {
				refEntity, err := r.FindEntity(ref.name)
				if err != nil {
					return fmt.Errorf("invalid reference %v.%v, %w", e.Name, a.Name, err)
				}
				ref.Entity = refEntity

				ref.Entity.InverseAttributes = append(ref.Entity.InverseAttributes, &Attribute{
					Name: e.Name,
					AsType: &Reference{
						Entity: e,
						name:   e.Name,
					},
					InverseName:     a.Name,
					InverseIdentity: a.Identity,
				})
			}
		}
	}

	return nil
}

func enumNames(r *AST) {
	for _, e := range r.Entities {
		for _, a := range e.Attributes {
			if enum, is := a.Type().(*Enum); is {
				for _, declaration := range r.Enums {
					if declaration.Name == enum.Name {
						enum.Type = declaration
					}
				}
			}
		}
	}
}
