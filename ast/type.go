package ast

type Type interface {
	IsAuto() bool
	String() string
}

type Int struct{}

func (i *Int) IsAuto() bool {
	return false
}

func (i *Int) String() string {
	return "int"
}

type Serial struct{}

func (s *Serial) IsAuto() bool {
	return true
}

func (s *Serial) String() string {
	return "serial"
}

type Float struct{}

func (f *Float) IsAuto() bool {
	return false
}

func (f *Float) String() string {
	return "float"
}

type String struct{}

func (s *String) IsAuto() bool {
	return false
}

func (s *String) String() string {
	return "string"
}

type Bool struct{}

func (b *Bool) IsAuto() bool {
	return false
}

func (b *Bool) String() string {
	return "bool"
}

type Enum struct {
	Name string
	Type *EnumType
}

func (e *Enum) IsAuto() bool {
	return false
}

func (e *Enum) String() string {
	return "enum"
}

func (e *Enum) Values() []EnumValue {
	return e.Type.Values
}

func (e *Enum) MaxLength() int {
	return e.Type.MaxLength()
}

func (e *Enum) Short() string {
	return e.Type.Short()
}

type Time struct{}

func (t *Time) IsAuto() bool {
	return false
}

func (t *Time) String() string {
	return "time"
}

type Reference struct {
	name   string
	Entity *Entity
}

func MkReference(name string) *Reference {
	return &Reference{name: name}
}

func (r *Reference) IsAuto() bool {
	return false
}

func (r *Reference) String() string {
	return "reference"
}
