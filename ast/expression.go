package ast

type Expression interface {
	Type() Type
}

type StringLiteral struct {
	Value string
}

func (s *StringLiteral) Type() Type {
	return &String{}
}

type IntLiteral struct {
	Value int
}

func (s *IntLiteral) Type() Type {
	return &Int{}
}

type Column struct {
	Entity *Entity
	Name   string
}

func (c *Column) Type() Type {
	for _, a := range c.Entity.Attributes {
		if a.Name == c.Name {
			return a.Type()
		}
	}

	panic("unknown column " + c.Name)
}

type Addition struct {
	Left  Expression
	Right Expression
}

func (a *Addition) Type() Type {
	leftType := a.Left.Type()
	_, leftIsStr := leftType.(*String)
	if leftIsStr {
		return leftType
	}

	panic("only string addition supported so far")
}

type Modulo struct {
	Left  Expression
	Right Expression
}

func (a *Modulo) Type() Type {
	if intLike(a.Left, a.Right) {
		return &Int{}
	}

	panic("modulo must have int parameters")
}

type Hash struct {
	Expression Expression
}

func (a *Hash) Type() Type {
	return &Int{}
}

type Abs struct {
	Expression Expression
}

func (a *Abs) Type() Type {
	if !intLike(a.Expression) {
		panic("abs must have int parameter")
	}
	return &Int{}
}

type Lower struct {
	Expression Expression
}

func (a *Lower) Type() Type {
	if !stringLike(a.Expression) {
		panic("lower must have string parameter")
	}
	return &String{}
}

type Upper struct {
	Expression Expression
}

func (a *Upper) Type() Type {
	if !stringLike(a.Expression) {
		panic("upper must have string parameter")
	}
	return &String{}
}

func intLike(xs ...Expression) bool {
	for _, x := range xs {
		tp := x.Type()
		_, isInt := tp.(*Int)
		if isInt {
			continue
		}
		_, isSer := tp.(*Serial)
		if isSer {
			continue
		}

		return false
	}

	return true
}

func stringLike(xs ...Expression) bool {
	for _, x := range xs {
		tp := x.Type()
		_, isInt := tp.(*String)
		if isInt {
			continue
		}

		return false
	}

	return true
}
