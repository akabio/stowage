package parser

import (
	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/stowage/ast"
)

func Parse(i string, sourceFolder string) (*ast.AST, error) {
	input := antlr.NewInputStream(i)
	lexer := NewStowageLexer(input)
	stream := antlr.NewCommonTokenStream(lexer, 0)

	p := NewStowageParser(stream)
	p.RemoveErrorListeners()
	antlrErrors := NewErrorListener()
	p.AddErrorListener(antlrErrors)
	p.BuildParseTrees = true
	root := p.Root()

	visi := &ASTVisitor{
		BaseStowageVisitor: &BaseStowageVisitor{},
		error: func(e error) {
			antlrErrors.errors = append(antlrErrors.errors, e)
		},
	}

	result := root.Accept(visi).(*ast.AST)

	err := ast.Normalize(result, sourceFolder)
	if err != nil {
		return nil, err
	}

	// d, _ := yaml.Marshal(result)
	// fmt.Println(string(d))

	return result, antlrErrors.Get()
}
