// Code generated from parser/Stowage.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // Stowage

import "github.com/antlr4-go/antlr/v4"

type BaseStowageVisitor struct {
	*antlr.BaseParseTreeVisitor
}

func (v *BaseStowageVisitor) VisitIdentifier(ctx *IdentifierContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitIdentifiers(ctx *IdentifiersContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitPkg(ctx *PkgContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitRoot(ctx *RootContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitEntity(ctx *EntityContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitView(ctx *ViewContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitQuery(ctx *QueryContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitEnum(ctx *EnumContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitAttribute(ctx *AttributeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitViewAttribute(ctx *ViewAttributeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitTypeDef(ctx *TypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitViewTypeDef(ctx *ViewTypeDefContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitComputedAttribute(ctx *ComputedAttributeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitIntType(ctx *IntTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitSerialType(ctx *SerialTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitFloatType(ctx *FloatTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitStringType(ctx *StringTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitBoolType(ctx *BoolTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitTimeType(ctx *TimeTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitReferenceType(ctx *ReferenceTypeContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitExpression(ctx *ExpressionContext) interface{} {
	return v.VisitChildren(ctx)
}

func (v *BaseStowageVisitor) VisitLiteral(ctx *LiteralContext) interface{} {
	return v.VisitChildren(ctx)
}
