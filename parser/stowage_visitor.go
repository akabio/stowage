// Code generated from parser/Stowage.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // Stowage

import "github.com/antlr4-go/antlr/v4"

// A complete Visitor for a parse tree produced by StowageParser.
type StowageVisitor interface {
	antlr.ParseTreeVisitor

	// Visit a parse tree produced by StowageParser#identifier.
	VisitIdentifier(ctx *IdentifierContext) interface{}

	// Visit a parse tree produced by StowageParser#identifiers.
	VisitIdentifiers(ctx *IdentifiersContext) interface{}

	// Visit a parse tree produced by StowageParser#pkg.
	VisitPkg(ctx *PkgContext) interface{}

	// Visit a parse tree produced by StowageParser#root.
	VisitRoot(ctx *RootContext) interface{}

	// Visit a parse tree produced by StowageParser#entity.
	VisitEntity(ctx *EntityContext) interface{}

	// Visit a parse tree produced by StowageParser#view.
	VisitView(ctx *ViewContext) interface{}

	// Visit a parse tree produced by StowageParser#query.
	VisitQuery(ctx *QueryContext) interface{}

	// Visit a parse tree produced by StowageParser#enum.
	VisitEnum(ctx *EnumContext) interface{}

	// Visit a parse tree produced by StowageParser#enumValue.
	VisitEnumValue(ctx *EnumValueContext) interface{}

	// Visit a parse tree produced by StowageParser#attribute.
	VisitAttribute(ctx *AttributeContext) interface{}

	// Visit a parse tree produced by StowageParser#viewAttribute.
	VisitViewAttribute(ctx *ViewAttributeContext) interface{}

	// Visit a parse tree produced by StowageParser#uniqueTypeDef.
	VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{}

	// Visit a parse tree produced by StowageParser#typeDef.
	VisitTypeDef(ctx *TypeDefContext) interface{}

	// Visit a parse tree produced by StowageParser#viewTypeDef.
	VisitViewTypeDef(ctx *ViewTypeDefContext) interface{}

	// Visit a parse tree produced by StowageParser#computedAttribute.
	VisitComputedAttribute(ctx *ComputedAttributeContext) interface{}

	// Visit a parse tree produced by StowageParser#intType.
	VisitIntType(ctx *IntTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#serialType.
	VisitSerialType(ctx *SerialTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#floatType.
	VisitFloatType(ctx *FloatTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#stringType.
	VisitStringType(ctx *StringTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#boolType.
	VisitBoolType(ctx *BoolTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#enumType.
	VisitEnumType(ctx *EnumTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#timeType.
	VisitTimeType(ctx *TimeTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#referenceType.
	VisitReferenceType(ctx *ReferenceTypeContext) interface{}

	// Visit a parse tree produced by StowageParser#expression.
	VisitExpression(ctx *ExpressionContext) interface{}

	// Visit a parse tree produced by StowageParser#literal.
	VisitLiteral(ctx *LiteralContext) interface{}
}
