package parser

import (
	"fmt"
	"strconv"

	"github.com/antlr4-go/antlr/v4"
	"gitlab.com/akabio/stowage/ast"
)

type ASTVisitor struct {
	*BaseStowageVisitor
	currentEntity *ast.Entity
	error         func(err error)
}

func (v *ASTVisitor) VisitRoot(ctx *RootContext) interface{} {
	a := &ast.AST{}

	for _, enumDec := range ctx.AllEnum() {
		e := enumDec.Accept(v).(*ast.EnumType)
		a.Enums = append(a.Enums, e)
	}

	for _, entityDec := range ctx.AllEntity() {
		e := entityDec.Accept(v).(*ast.Entity)
		a.Entities = append(a.Entities, e)
	}

	for _, viewDec := range ctx.AllView() {
		e := viewDec.Accept(v).(*ast.Entity)
		a.Entities = append(a.Entities, e)
	}

	for _, queryDec := range ctx.AllQuery() {
		e := queryDec.Accept(v).(*ast.Entity)
		a.Entities = append(a.Entities, e)
	}

	return a
}

func (v *ASTVisitor) VisitIdentifiers(ctx *IdentifiersContext) interface{} {
	ids := []string{}
	for _, id := range ctx.AllIdentifier() {
		ids = append(ids, id.GetText())
	}
	return ids
}

func (v *ASTVisitor) VisitEntity(ctx *EntityContext) interface{} {
	v.currentEntity = v.makeEntity(ctx)

	// find out identity attributes
	ids := []string{}
	if ctx.Identifiers() != nil {
		ids = ctx.Identifiers().Accept(v).([]string)
	}

	idm := map[string]bool{}
	for _, id := range ids {
		idm[id] = true
	}

	// if there are no identity attributes, add one called id
	if len(ids) == 0 {
		v.currentEntity.Attributes = append(v.currentEntity.Attributes, &ast.Attribute{
			Name:     "id",
			AsType:   &ast.Serial{},
			Identity: true,
		})
	}

	for _, a := range ctx.AllAttribute() {
		at := a.Accept(v).(*ast.Attribute)
		if idm[at.Name] {
			at.Identity = true
			delete(idm, at.Name)
		}
		v.currentEntity.Attributes = append(v.currentEntity.Attributes, at)
	}

	for k := range idm { // check if we found all identity attributes
		v.error(fmt.Errorf("identity attribute %v not defined for entity %v", k, v.currentEntity.Name))
	}

	e := v.currentEntity
	v.currentEntity = nil

	return e
}

func (v *ASTVisitor) VisitView(ctx *ViewContext) interface{} {
	e := v.makeEntity(ctx)
	e.IsView = true

	for _, a := range ctx.AllViewAttribute() {
		at := a.Accept(v).(*ast.Attribute)
		e.Attributes = append(e.Attributes, at)
	}

	return e
}

func (v *ASTVisitor) VisitQuery(ctx *QueryContext) interface{} {
	e := v.makeEntity(ctx)
	e.IsQuery = true

	for _, a := range ctx.AllViewAttribute() {
		at := a.Accept(v).(*ast.Attribute)
		e.Attributes = append(e.Attributes, at)
	}

	return e
}

func (v *ASTVisitor) makeEntity(ctx interface {
	AllCOMMENT() []antlr.TerminalNode
	Identifier() IIdentifierContext
},
) *ast.Entity {
	comments := []string{}
	for _, cmt := range ctx.AllCOMMENT() {
		comments = append(comments, cmt.GetText()[2:])
	}
	return &ast.Entity{
		Name:     ctx.Identifier().GetText(),
		Comments: comments,
	}
}

func (v *ASTVisitor) VisitEnum(ctx *EnumContext) any {
	en := &ast.EnumType{
		Name: ctx.Identifier().GetText(),
	}

	for _, val := range ctx.AllEnumValue() {
		ev := val.Accept(v).(ast.EnumValue)
		en.Values = append(en.Values, ev)
	}

	return en
}

func (v *ASTVisitor) VisitEnumValue(ctx *EnumValueContext) interface{} {
	id := ctx.Identifier().GetText()
	value := id
	if ctx.STRING() != nil {
		value = ctx.STRING().GetText()
		value = value[1 : len(value)-1]
	}
	return ast.EnumValue{
		ID:    id,
		Value: value,
	}
}

func (v *ASTVisitor) VisitAttribute(ctx *AttributeContext) interface{} {
	att := &ast.Attribute{
		Name: ctx.Identifier().GetText(),
	}

	if ctx.TypeDef() != nil {
		att.AsType = ctx.TypeDef().Accept(v).(ast.Type)
	}
	if ctx.UniqueTypeDef() != nil {
		att.AsType = ctx.UniqueTypeDef().Accept(v).(ast.Type)
		att.Unique = ctx.UNIQUE() != nil
	}
	if ctx.ComputedAttribute() != nil {
		att.Computed = ctx.ComputedAttribute().Expression().Accept(v).(ast.Expression)
	}

	return att
}

func (v *ASTVisitor) VisitViewAttribute(ctx *ViewAttributeContext) interface{} {
	att := &ast.Attribute{
		Name: ctx.Identifier().GetText(),
	}

	if ctx.ViewTypeDef() != nil {
		att.AsType = ctx.ViewTypeDef().Accept(v).(ast.Type)
	}

	return att
}

func (v *ASTVisitor) VisitUniqueTypeDef(ctx *UniqueTypeDefContext) interface{} {
	if ctx.IntType() != nil {
		return ctx.IntType().Accept(v)
	}
	if ctx.FloatType() != nil {
		return ctx.FloatType().Accept(v)
	}
	if ctx.StringType() != nil {
		return ctx.StringType().Accept(v)
	}
	if ctx.TimeType() != nil {
		return ctx.TimeType().Accept(v)
	}

	panic("no type def")
}

func (v *ASTVisitor) VisitTypeDef(ctx *TypeDefContext) interface{} {
	if ctx.SerialType() != nil {
		return ctx.SerialType().Accept(v)
	}
	if ctx.BoolType() != nil {
		return ctx.BoolType().Accept(v)
	}
	if ctx.ReferenceType() != nil {
		return ctx.ReferenceType().Accept(v)
	}
	if ctx.EnumType() != nil {
		return ctx.EnumType().Accept(v)
	}

	panic("no type def")
}

func (v *ASTVisitor) VisitViewTypeDef(ctx *ViewTypeDefContext) interface{} {
	if ctx.IntType() != nil {
		return ctx.IntType().Accept(v)
	}
	if ctx.FloatType() != nil {
		return ctx.FloatType().Accept(v)
	}
	if ctx.StringType() != nil {
		return ctx.StringType().Accept(v)
	}
	if ctx.TimeType() != nil {
		return ctx.TimeType().Accept(v)
	}
	if ctx.BoolType() != nil {
		return ctx.BoolType().Accept(v)
	}
	if ctx.ReferenceType() != nil {
		return ctx.ReferenceType().Accept(v)
	}
	if ctx.EnumType() != nil {
		return ctx.EnumType().Accept(v)
	}

	panic("no type def")
}

func (v *ASTVisitor) VisitIntType(_ *IntTypeContext) interface{} {
	return &ast.Int{}
}

func (v *ASTVisitor) VisitSerialType(_ *SerialTypeContext) interface{} {
	return &ast.Serial{}
}

func (v *ASTVisitor) VisitStringType(_ *StringTypeContext) interface{} {
	return &ast.String{}
}

func (v *ASTVisitor) VisitFloatType(_ *FloatTypeContext) interface{} {
	return &ast.Float{}
}

func (v *ASTVisitor) VisitBoolType(_ *BoolTypeContext) interface{} {
	return &ast.Bool{}
}

func (v *ASTVisitor) VisitEnumType(ctx *EnumTypeContext) interface{} {
	en := &ast.Enum{
		Name: ctx.Identifier().GetText(),
	}
	return en
}

func (v *ASTVisitor) VisitTimeType(_ *TimeTypeContext) interface{} {
	return &ast.Time{}
}

func (v *ASTVisitor) VisitReferenceType(ctx *ReferenceTypeContext) interface{} {
	return ast.MkReference(
		ctx.Identifier().GetText(),
	)
}

func (v *ASTVisitor) VisitExpression(ctx *ExpressionContext) interface{} {
	if ctx.Literal() != nil {
		return ctx.Literal().Accept(v).(ast.Expression)
	}

	if ctx.GetAddition() != nil {
		return &ast.Addition{
			Left:  ctx.Expression(0).Accept(v).(ast.Expression),
			Right: ctx.Expression(1).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetModulo() != nil {
		return &ast.Modulo{
			Left:  ctx.Expression(0).Accept(v).(ast.Expression),
			Right: ctx.Expression(1).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetHash() != nil {
		return &ast.Hash{
			Expression: ctx.Expression(0).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetAbs() != nil {
		return &ast.Abs{
			Expression: ctx.Expression(0).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetLower() != nil {
		return &ast.Lower{
			Expression: ctx.Expression(0).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetUpper() != nil {
		return &ast.Upper{
			Expression: ctx.Expression(0).Accept(v).(ast.Expression),
		}
	}

	if ctx.GetColumn() != nil {
		return &ast.Column{
			Name:   ctx.GetColumn().GetText(),
			Entity: v.currentEntity,
		}
	}

	panic(fmt.Sprint("invalid expression", ctx))
}

func (v *ASTVisitor) VisitLiteral(ctx *LiteralContext) interface{} {
	if ctx.STRING() != nil {
		value := ctx.STRING().GetText()
		value = value[1 : len(value)-1]
		return &ast.StringLiteral{Value: value}
	}

	if ctx.INT() != nil {
		value := ctx.INT().GetText()
		i, err := strconv.ParseInt(value, 10, 64)
		if err != nil {
			panic(err)
		}

		return &ast.IntLiteral{Value: int(i)}
	}
	panic("invalid literal")
}
