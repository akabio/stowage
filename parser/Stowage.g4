grammar Stowage;

identifier:
	IDENTIFIER
	| 'int'
	| 'float'
	| 'string'
	| 'enum'
	| 'bool'
	| 'time'
	// keywords
	| 'entity'
	| 'view'
	| 'query'
	| UNIQUE
	| 'identified'
	| 'by'
	| HASH
	| ABS
	| LOWER
	| UPPER
	| COMPUTED
	| 'name';

identifiers: identifier (',' identifier)*;

pkg: identifier (('.' | '/') identifier)*;

root: (entity | view | query | enum)* EOF;

entity:
	COMMENT* 'entity' identifier ('identified' 'by' identifiers)? '{' attribute* '}';

view:
	COMMENT* 'view' identifier '{' viewAttribute* '}';

query:
	COMMENT* 'query' identifier '{' viewAttribute* '}';

enum:
	COMMENT* 'enum' identifier '(' enumValue (',' enumValue)+ ')';

enumValue: identifier ('=' STRING)?;

attribute: identifier (UNIQUE? uniqueTypeDef | typeDef | computedAttribute);

viewAttribute: identifier (viewTypeDef);

uniqueTypeDef: intType | floatType | stringType | timeType;

typeDef: serialType | boolType | enumType | referenceType;

viewTypeDef: intType | floatType | stringType | timeType | boolType | enumType | referenceType;

computedAttribute: COMPUTED expression;

intType: 'int';

serialType: 'serial';

floatType: 'float';

stringType: 'string';

boolType: 'bool';

enumType: 'enum' identifier;

timeType: 'time';

referenceType: '->' identifier;

expression:
					| expression addition='+' expression
					| expression modulo='%' expression
					| literal
					| hash=HASH '(' expression ')'
					| abs=ABS '(' expression ')'
					| lower=LOWER '(' expression ')'
					| upper=UPPER '(' expression ')'
					| column=identifier
					;

literal: STRING | INT;

COL: ':';

COMMENT: '//' ~( '\r' | '\n')*;

UNIQUE: 'unique';
COMPUTED: 'computed';

HASH: 'hash';
ABS: 'abs';
LOWER: 'lower';
UPPER: 'upper';

IDENTIFIER: [a-z][a-zA-Z0-9]*;

STRING: '"' .*? '"';
INT: '-'? [0-9]+;

WHITESPACE: [ \r\n\t]+ -> skip;