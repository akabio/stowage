package parser

import (
	"fmt"
	"testing"

	"github.com/ghodss/yaml"
)

func TestParseIndent(t *testing.T) {
	ast, err := Parse(`
	name: example

	interfacePackage: gitlab.com/akabio/stowage/test/example/store
	postgresPackage: gitlab.com/akabio/stowage/test/example/postgres

	entity document {
		count int
		name string
		author -> user
	}

	entity user {
		login unique string
		firstName string
		lastName string
	}

	entity role identified by name {
		name unique string
	}
	`, "./")
	fmt.Println(err)
	y, err := yaml.Marshal(ast)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(y))
}
