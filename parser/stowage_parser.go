// Code generated from parser/Stowage.g4 by ANTLR 4.13.2. DO NOT EDIT.

package parser // Stowage

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/antlr4-go/antlr/v4"
)

// Suppress unused import errors
var _ = fmt.Printf
var _ = strconv.Itoa
var _ = sync.Once{}

type StowageParser struct {
	*antlr.BaseParser
}

var StowageParserStaticData struct {
	once                   sync.Once
	serializedATN          []int32
	LiteralNames           []string
	SymbolicNames          []string
	RuleNames              []string
	PredictionContextCache *antlr.PredictionContextCache
	atn                    *antlr.ATN
	decisionToDFA          []*antlr.DFA
}

func stowageParserInit() {
	staticData := &StowageParserStaticData
	staticData.LiteralNames = []string{
		"", "'int'", "'float'", "'string'", "'enum'", "'bool'", "'time'", "'entity'",
		"'view'", "'query'", "'identified'", "'by'", "'name'", "','", "'.'",
		"'/'", "'{'", "'}'", "'('", "')'", "'='", "'serial'", "'->'", "'+'",
		"'%'", "':'", "", "'unique'", "'computed'", "'hash'", "'abs'", "'lower'",
		"'upper'",
	}
	staticData.SymbolicNames = []string{
		"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
		"", "", "", "", "", "", "", "", "COL", "COMMENT", "UNIQUE", "COMPUTED",
		"HASH", "ABS", "LOWER", "UPPER", "IDENTIFIER", "STRING", "INT", "WHITESPACE",
	}
	staticData.RuleNames = []string{
		"identifier", "identifiers", "pkg", "root", "entity", "view", "query",
		"enum", "enumValue", "attribute", "viewAttribute", "uniqueTypeDef",
		"typeDef", "viewTypeDef", "computedAttribute", "intType", "serialType",
		"floatType", "stringType", "boolType", "enumType", "timeType", "referenceType",
		"expression", "literal",
	}
	staticData.PredictionContextCache = antlr.NewPredictionContextCache()
	staticData.serializedATN = []int32{
		4, 1, 36, 251, 2, 0, 7, 0, 2, 1, 7, 1, 2, 2, 7, 2, 2, 3, 7, 3, 2, 4, 7,
		4, 2, 5, 7, 5, 2, 6, 7, 6, 2, 7, 7, 7, 2, 8, 7, 8, 2, 9, 7, 9, 2, 10, 7,
		10, 2, 11, 7, 11, 2, 12, 7, 12, 2, 13, 7, 13, 2, 14, 7, 14, 2, 15, 7, 15,
		2, 16, 7, 16, 2, 17, 7, 17, 2, 18, 7, 18, 2, 19, 7, 19, 2, 20, 7, 20, 2,
		21, 7, 21, 2, 22, 7, 22, 2, 23, 7, 23, 2, 24, 7, 24, 1, 0, 1, 0, 1, 1,
		1, 1, 1, 1, 5, 1, 56, 8, 1, 10, 1, 12, 1, 59, 9, 1, 1, 2, 1, 2, 1, 2, 5,
		2, 64, 8, 2, 10, 2, 12, 2, 67, 9, 2, 1, 3, 1, 3, 1, 3, 1, 3, 5, 3, 73,
		8, 3, 10, 3, 12, 3, 76, 9, 3, 1, 3, 1, 3, 1, 4, 5, 4, 81, 8, 4, 10, 4,
		12, 4, 84, 9, 4, 1, 4, 1, 4, 1, 4, 1, 4, 1, 4, 3, 4, 91, 8, 4, 1, 4, 1,
		4, 5, 4, 95, 8, 4, 10, 4, 12, 4, 98, 9, 4, 1, 4, 1, 4, 1, 5, 5, 5, 103,
		8, 5, 10, 5, 12, 5, 106, 9, 5, 1, 5, 1, 5, 1, 5, 1, 5, 5, 5, 112, 8, 5,
		10, 5, 12, 5, 115, 9, 5, 1, 5, 1, 5, 1, 6, 5, 6, 120, 8, 6, 10, 6, 12,
		6, 123, 9, 6, 1, 6, 1, 6, 1, 6, 1, 6, 5, 6, 129, 8, 6, 10, 6, 12, 6, 132,
		9, 6, 1, 6, 1, 6, 1, 7, 5, 7, 137, 8, 7, 10, 7, 12, 7, 140, 9, 7, 1, 7,
		1, 7, 1, 7, 1, 7, 1, 7, 1, 7, 4, 7, 148, 8, 7, 11, 7, 12, 7, 149, 1, 7,
		1, 7, 1, 8, 1, 8, 1, 8, 3, 8, 157, 8, 8, 1, 9, 1, 9, 3, 9, 161, 8, 9, 1,
		9, 1, 9, 1, 9, 3, 9, 166, 8, 9, 1, 10, 1, 10, 1, 10, 1, 11, 1, 11, 1, 11,
		1, 11, 3, 11, 175, 8, 11, 1, 12, 1, 12, 1, 12, 1, 12, 3, 12, 181, 8, 12,
		1, 13, 1, 13, 1, 13, 1, 13, 1, 13, 1, 13, 1, 13, 3, 13, 190, 8, 13, 1,
		14, 1, 14, 1, 14, 1, 15, 1, 15, 1, 16, 1, 16, 1, 17, 1, 17, 1, 18, 1, 18,
		1, 19, 1, 19, 1, 20, 1, 20, 1, 20, 1, 21, 1, 21, 1, 22, 1, 22, 1, 22, 1,
		23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23,
		1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1,
		23, 1, 23, 3, 23, 236, 8, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23, 1, 23,
		5, 23, 244, 8, 23, 10, 23, 12, 23, 247, 9, 23, 1, 24, 1, 24, 1, 24, 0,
		1, 46, 25, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32,
		34, 36, 38, 40, 42, 44, 46, 48, 0, 3, 2, 0, 1, 12, 27, 33, 1, 0, 14, 15,
		1, 0, 34, 35, 264, 0, 50, 1, 0, 0, 0, 2, 52, 1, 0, 0, 0, 4, 60, 1, 0, 0,
		0, 6, 74, 1, 0, 0, 0, 8, 82, 1, 0, 0, 0, 10, 104, 1, 0, 0, 0, 12, 121,
		1, 0, 0, 0, 14, 138, 1, 0, 0, 0, 16, 153, 1, 0, 0, 0, 18, 158, 1, 0, 0,
		0, 20, 167, 1, 0, 0, 0, 22, 174, 1, 0, 0, 0, 24, 180, 1, 0, 0, 0, 26, 189,
		1, 0, 0, 0, 28, 191, 1, 0, 0, 0, 30, 194, 1, 0, 0, 0, 32, 196, 1, 0, 0,
		0, 34, 198, 1, 0, 0, 0, 36, 200, 1, 0, 0, 0, 38, 202, 1, 0, 0, 0, 40, 204,
		1, 0, 0, 0, 42, 207, 1, 0, 0, 0, 44, 209, 1, 0, 0, 0, 46, 235, 1, 0, 0,
		0, 48, 248, 1, 0, 0, 0, 50, 51, 7, 0, 0, 0, 51, 1, 1, 0, 0, 0, 52, 57,
		3, 0, 0, 0, 53, 54, 5, 13, 0, 0, 54, 56, 3, 0, 0, 0, 55, 53, 1, 0, 0, 0,
		56, 59, 1, 0, 0, 0, 57, 55, 1, 0, 0, 0, 57, 58, 1, 0, 0, 0, 58, 3, 1, 0,
		0, 0, 59, 57, 1, 0, 0, 0, 60, 65, 3, 0, 0, 0, 61, 62, 7, 1, 0, 0, 62, 64,
		3, 0, 0, 0, 63, 61, 1, 0, 0, 0, 64, 67, 1, 0, 0, 0, 65, 63, 1, 0, 0, 0,
		65, 66, 1, 0, 0, 0, 66, 5, 1, 0, 0, 0, 67, 65, 1, 0, 0, 0, 68, 73, 3, 8,
		4, 0, 69, 73, 3, 10, 5, 0, 70, 73, 3, 12, 6, 0, 71, 73, 3, 14, 7, 0, 72,
		68, 1, 0, 0, 0, 72, 69, 1, 0, 0, 0, 72, 70, 1, 0, 0, 0, 72, 71, 1, 0, 0,
		0, 73, 76, 1, 0, 0, 0, 74, 72, 1, 0, 0, 0, 74, 75, 1, 0, 0, 0, 75, 77,
		1, 0, 0, 0, 76, 74, 1, 0, 0, 0, 77, 78, 5, 0, 0, 1, 78, 7, 1, 0, 0, 0,
		79, 81, 5, 26, 0, 0, 80, 79, 1, 0, 0, 0, 81, 84, 1, 0, 0, 0, 82, 80, 1,
		0, 0, 0, 82, 83, 1, 0, 0, 0, 83, 85, 1, 0, 0, 0, 84, 82, 1, 0, 0, 0, 85,
		86, 5, 7, 0, 0, 86, 90, 3, 0, 0, 0, 87, 88, 5, 10, 0, 0, 88, 89, 5, 11,
		0, 0, 89, 91, 3, 2, 1, 0, 90, 87, 1, 0, 0, 0, 90, 91, 1, 0, 0, 0, 91, 92,
		1, 0, 0, 0, 92, 96, 5, 16, 0, 0, 93, 95, 3, 18, 9, 0, 94, 93, 1, 0, 0,
		0, 95, 98, 1, 0, 0, 0, 96, 94, 1, 0, 0, 0, 96, 97, 1, 0, 0, 0, 97, 99,
		1, 0, 0, 0, 98, 96, 1, 0, 0, 0, 99, 100, 5, 17, 0, 0, 100, 9, 1, 0, 0,
		0, 101, 103, 5, 26, 0, 0, 102, 101, 1, 0, 0, 0, 103, 106, 1, 0, 0, 0, 104,
		102, 1, 0, 0, 0, 104, 105, 1, 0, 0, 0, 105, 107, 1, 0, 0, 0, 106, 104,
		1, 0, 0, 0, 107, 108, 5, 8, 0, 0, 108, 109, 3, 0, 0, 0, 109, 113, 5, 16,
		0, 0, 110, 112, 3, 20, 10, 0, 111, 110, 1, 0, 0, 0, 112, 115, 1, 0, 0,
		0, 113, 111, 1, 0, 0, 0, 113, 114, 1, 0, 0, 0, 114, 116, 1, 0, 0, 0, 115,
		113, 1, 0, 0, 0, 116, 117, 5, 17, 0, 0, 117, 11, 1, 0, 0, 0, 118, 120,
		5, 26, 0, 0, 119, 118, 1, 0, 0, 0, 120, 123, 1, 0, 0, 0, 121, 119, 1, 0,
		0, 0, 121, 122, 1, 0, 0, 0, 122, 124, 1, 0, 0, 0, 123, 121, 1, 0, 0, 0,
		124, 125, 5, 9, 0, 0, 125, 126, 3, 0, 0, 0, 126, 130, 5, 16, 0, 0, 127,
		129, 3, 20, 10, 0, 128, 127, 1, 0, 0, 0, 129, 132, 1, 0, 0, 0, 130, 128,
		1, 0, 0, 0, 130, 131, 1, 0, 0, 0, 131, 133, 1, 0, 0, 0, 132, 130, 1, 0,
		0, 0, 133, 134, 5, 17, 0, 0, 134, 13, 1, 0, 0, 0, 135, 137, 5, 26, 0, 0,
		136, 135, 1, 0, 0, 0, 137, 140, 1, 0, 0, 0, 138, 136, 1, 0, 0, 0, 138,
		139, 1, 0, 0, 0, 139, 141, 1, 0, 0, 0, 140, 138, 1, 0, 0, 0, 141, 142,
		5, 4, 0, 0, 142, 143, 3, 0, 0, 0, 143, 144, 5, 18, 0, 0, 144, 147, 3, 16,
		8, 0, 145, 146, 5, 13, 0, 0, 146, 148, 3, 16, 8, 0, 147, 145, 1, 0, 0,
		0, 148, 149, 1, 0, 0, 0, 149, 147, 1, 0, 0, 0, 149, 150, 1, 0, 0, 0, 150,
		151, 1, 0, 0, 0, 151, 152, 5, 19, 0, 0, 152, 15, 1, 0, 0, 0, 153, 156,
		3, 0, 0, 0, 154, 155, 5, 20, 0, 0, 155, 157, 5, 34, 0, 0, 156, 154, 1,
		0, 0, 0, 156, 157, 1, 0, 0, 0, 157, 17, 1, 0, 0, 0, 158, 165, 3, 0, 0,
		0, 159, 161, 5, 27, 0, 0, 160, 159, 1, 0, 0, 0, 160, 161, 1, 0, 0, 0, 161,
		162, 1, 0, 0, 0, 162, 166, 3, 22, 11, 0, 163, 166, 3, 24, 12, 0, 164, 166,
		3, 28, 14, 0, 165, 160, 1, 0, 0, 0, 165, 163, 1, 0, 0, 0, 165, 164, 1,
		0, 0, 0, 166, 19, 1, 0, 0, 0, 167, 168, 3, 0, 0, 0, 168, 169, 3, 26, 13,
		0, 169, 21, 1, 0, 0, 0, 170, 175, 3, 30, 15, 0, 171, 175, 3, 34, 17, 0,
		172, 175, 3, 36, 18, 0, 173, 175, 3, 42, 21, 0, 174, 170, 1, 0, 0, 0, 174,
		171, 1, 0, 0, 0, 174, 172, 1, 0, 0, 0, 174, 173, 1, 0, 0, 0, 175, 23, 1,
		0, 0, 0, 176, 181, 3, 32, 16, 0, 177, 181, 3, 38, 19, 0, 178, 181, 3, 40,
		20, 0, 179, 181, 3, 44, 22, 0, 180, 176, 1, 0, 0, 0, 180, 177, 1, 0, 0,
		0, 180, 178, 1, 0, 0, 0, 180, 179, 1, 0, 0, 0, 181, 25, 1, 0, 0, 0, 182,
		190, 3, 30, 15, 0, 183, 190, 3, 34, 17, 0, 184, 190, 3, 36, 18, 0, 185,
		190, 3, 42, 21, 0, 186, 190, 3, 38, 19, 0, 187, 190, 3, 40, 20, 0, 188,
		190, 3, 44, 22, 0, 189, 182, 1, 0, 0, 0, 189, 183, 1, 0, 0, 0, 189, 184,
		1, 0, 0, 0, 189, 185, 1, 0, 0, 0, 189, 186, 1, 0, 0, 0, 189, 187, 1, 0,
		0, 0, 189, 188, 1, 0, 0, 0, 190, 27, 1, 0, 0, 0, 191, 192, 5, 28, 0, 0,
		192, 193, 3, 46, 23, 0, 193, 29, 1, 0, 0, 0, 194, 195, 5, 1, 0, 0, 195,
		31, 1, 0, 0, 0, 196, 197, 5, 21, 0, 0, 197, 33, 1, 0, 0, 0, 198, 199, 5,
		2, 0, 0, 199, 35, 1, 0, 0, 0, 200, 201, 5, 3, 0, 0, 201, 37, 1, 0, 0, 0,
		202, 203, 5, 5, 0, 0, 203, 39, 1, 0, 0, 0, 204, 205, 5, 4, 0, 0, 205, 206,
		3, 0, 0, 0, 206, 41, 1, 0, 0, 0, 207, 208, 5, 6, 0, 0, 208, 43, 1, 0, 0,
		0, 209, 210, 5, 22, 0, 0, 210, 211, 3, 0, 0, 0, 211, 45, 1, 0, 0, 0, 212,
		236, 6, 23, -1, 0, 213, 236, 3, 48, 24, 0, 214, 215, 5, 29, 0, 0, 215,
		216, 5, 18, 0, 0, 216, 217, 3, 46, 23, 0, 217, 218, 5, 19, 0, 0, 218, 236,
		1, 0, 0, 0, 219, 220, 5, 30, 0, 0, 220, 221, 5, 18, 0, 0, 221, 222, 3,
		46, 23, 0, 222, 223, 5, 19, 0, 0, 223, 236, 1, 0, 0, 0, 224, 225, 5, 31,
		0, 0, 225, 226, 5, 18, 0, 0, 226, 227, 3, 46, 23, 0, 227, 228, 5, 19, 0,
		0, 228, 236, 1, 0, 0, 0, 229, 230, 5, 32, 0, 0, 230, 231, 5, 18, 0, 0,
		231, 232, 3, 46, 23, 0, 232, 233, 5, 19, 0, 0, 233, 236, 1, 0, 0, 0, 234,
		236, 3, 0, 0, 0, 235, 212, 1, 0, 0, 0, 235, 213, 1, 0, 0, 0, 235, 214,
		1, 0, 0, 0, 235, 219, 1, 0, 0, 0, 235, 224, 1, 0, 0, 0, 235, 229, 1, 0,
		0, 0, 235, 234, 1, 0, 0, 0, 236, 245, 1, 0, 0, 0, 237, 238, 10, 8, 0, 0,
		238, 239, 5, 23, 0, 0, 239, 244, 3, 46, 23, 9, 240, 241, 10, 7, 0, 0, 241,
		242, 5, 24, 0, 0, 242, 244, 3, 46, 23, 8, 243, 237, 1, 0, 0, 0, 243, 240,
		1, 0, 0, 0, 244, 247, 1, 0, 0, 0, 245, 243, 1, 0, 0, 0, 245, 246, 1, 0,
		0, 0, 246, 47, 1, 0, 0, 0, 247, 245, 1, 0, 0, 0, 248, 249, 7, 2, 0, 0,
		249, 49, 1, 0, 0, 0, 22, 57, 65, 72, 74, 82, 90, 96, 104, 113, 121, 130,
		138, 149, 156, 160, 165, 174, 180, 189, 235, 243, 245,
	}
	deserializer := antlr.NewATNDeserializer(nil)
	staticData.atn = deserializer.Deserialize(staticData.serializedATN)
	atn := staticData.atn
	staticData.decisionToDFA = make([]*antlr.DFA, len(atn.DecisionToState))
	decisionToDFA := staticData.decisionToDFA
	for index, state := range atn.DecisionToState {
		decisionToDFA[index] = antlr.NewDFA(state, index)
	}
}

// StowageParserInit initializes any static state used to implement StowageParser. By default the
// static state used to implement the parser is lazily initialized during the first call to
// NewStowageParser(). You can call this function if you wish to initialize the static state ahead
// of time.
func StowageParserInit() {
	staticData := &StowageParserStaticData
	staticData.once.Do(stowageParserInit)
}

// NewStowageParser produces a new parser instance for the optional input antlr.TokenStream.
func NewStowageParser(input antlr.TokenStream) *StowageParser {
	StowageParserInit()
	this := new(StowageParser)
	this.BaseParser = antlr.NewBaseParser(input)
	staticData := &StowageParserStaticData
	this.Interpreter = antlr.NewParserATNSimulator(this, staticData.atn, staticData.decisionToDFA, staticData.PredictionContextCache)
	this.RuleNames = staticData.RuleNames
	this.LiteralNames = staticData.LiteralNames
	this.SymbolicNames = staticData.SymbolicNames
	this.GrammarFileName = "Stowage.g4"

	return this
}

// StowageParser tokens.
const (
	StowageParserEOF        = antlr.TokenEOF
	StowageParserT__0       = 1
	StowageParserT__1       = 2
	StowageParserT__2       = 3
	StowageParserT__3       = 4
	StowageParserT__4       = 5
	StowageParserT__5       = 6
	StowageParserT__6       = 7
	StowageParserT__7       = 8
	StowageParserT__8       = 9
	StowageParserT__9       = 10
	StowageParserT__10      = 11
	StowageParserT__11      = 12
	StowageParserT__12      = 13
	StowageParserT__13      = 14
	StowageParserT__14      = 15
	StowageParserT__15      = 16
	StowageParserT__16      = 17
	StowageParserT__17      = 18
	StowageParserT__18      = 19
	StowageParserT__19      = 20
	StowageParserT__20      = 21
	StowageParserT__21      = 22
	StowageParserT__22      = 23
	StowageParserT__23      = 24
	StowageParserCOL        = 25
	StowageParserCOMMENT    = 26
	StowageParserUNIQUE     = 27
	StowageParserCOMPUTED   = 28
	StowageParserHASH       = 29
	StowageParserABS        = 30
	StowageParserLOWER      = 31
	StowageParserUPPER      = 32
	StowageParserIDENTIFIER = 33
	StowageParserSTRING     = 34
	StowageParserINT        = 35
	StowageParserWHITESPACE = 36
)

// StowageParser rules.
const (
	StowageParserRULE_identifier        = 0
	StowageParserRULE_identifiers       = 1
	StowageParserRULE_pkg               = 2
	StowageParserRULE_root              = 3
	StowageParserRULE_entity            = 4
	StowageParserRULE_view              = 5
	StowageParserRULE_query             = 6
	StowageParserRULE_enum              = 7
	StowageParserRULE_enumValue         = 8
	StowageParserRULE_attribute         = 9
	StowageParserRULE_viewAttribute     = 10
	StowageParserRULE_uniqueTypeDef     = 11
	StowageParserRULE_typeDef           = 12
	StowageParserRULE_viewTypeDef       = 13
	StowageParserRULE_computedAttribute = 14
	StowageParserRULE_intType           = 15
	StowageParserRULE_serialType        = 16
	StowageParserRULE_floatType         = 17
	StowageParserRULE_stringType        = 18
	StowageParserRULE_boolType          = 19
	StowageParserRULE_enumType          = 20
	StowageParserRULE_timeType          = 21
	StowageParserRULE_referenceType     = 22
	StowageParserRULE_expression        = 23
	StowageParserRULE_literal           = 24
)

// IIdentifierContext is an interface to support dynamic dispatch.
type IIdentifierContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IDENTIFIER() antlr.TerminalNode
	UNIQUE() antlr.TerminalNode
	HASH() antlr.TerminalNode
	ABS() antlr.TerminalNode
	LOWER() antlr.TerminalNode
	UPPER() antlr.TerminalNode
	COMPUTED() antlr.TerminalNode

	// IsIdentifierContext differentiates from other interfaces.
	IsIdentifierContext()
}

type IdentifierContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifierContext() *IdentifierContext {
	var p = new(IdentifierContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_identifier
	return p
}

func InitEmptyIdentifierContext(p *IdentifierContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_identifier
}

func (*IdentifierContext) IsIdentifierContext() {}

func NewIdentifierContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifierContext {
	var p = new(IdentifierContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_identifier

	return p
}

func (s *IdentifierContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifierContext) IDENTIFIER() antlr.TerminalNode {
	return s.GetToken(StowageParserIDENTIFIER, 0)
}

func (s *IdentifierContext) UNIQUE() antlr.TerminalNode {
	return s.GetToken(StowageParserUNIQUE, 0)
}

func (s *IdentifierContext) HASH() antlr.TerminalNode {
	return s.GetToken(StowageParserHASH, 0)
}

func (s *IdentifierContext) ABS() antlr.TerminalNode {
	return s.GetToken(StowageParserABS, 0)
}

func (s *IdentifierContext) LOWER() antlr.TerminalNode {
	return s.GetToken(StowageParserLOWER, 0)
}

func (s *IdentifierContext) UPPER() antlr.TerminalNode {
	return s.GetToken(StowageParserUPPER, 0)
}

func (s *IdentifierContext) COMPUTED() antlr.TerminalNode {
	return s.GetToken(StowageParserCOMPUTED, 0)
}

func (s *IdentifierContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifierContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifierContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitIdentifier(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Identifier() (localctx IIdentifierContext) {
	localctx = NewIdentifierContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 0, StowageParserRULE_identifier)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(50)
		_la = p.GetTokenStream().LA(1)

		if !((int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&17045659646) != 0) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIdentifiersContext is an interface to support dynamic dispatch.
type IIdentifiersContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllIdentifier() []IIdentifierContext
	Identifier(i int) IIdentifierContext

	// IsIdentifiersContext differentiates from other interfaces.
	IsIdentifiersContext()
}

type IdentifiersContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIdentifiersContext() *IdentifiersContext {
	var p = new(IdentifiersContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_identifiers
	return p
}

func InitEmptyIdentifiersContext(p *IdentifiersContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_identifiers
}

func (*IdentifiersContext) IsIdentifiersContext() {}

func NewIdentifiersContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IdentifiersContext {
	var p = new(IdentifiersContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_identifiers

	return p
}

func (s *IdentifiersContext) GetParser() antlr.Parser { return s.parser }

func (s *IdentifiersContext) AllIdentifier() []IIdentifierContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IIdentifierContext); ok {
			len++
		}
	}

	tst := make([]IIdentifierContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IIdentifierContext); ok {
			tst[i] = t.(IIdentifierContext)
			i++
		}
	}

	return tst
}

func (s *IdentifiersContext) Identifier(i int) IIdentifierContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *IdentifiersContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IdentifiersContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IdentifiersContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitIdentifiers(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Identifiers() (localctx IIdentifiersContext) {
	localctx = NewIdentifiersContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 2, StowageParserRULE_identifiers)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(52)
		p.Identifier()
	}
	p.SetState(57)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserT__12 {
		{
			p.SetState(53)
			p.Match(StowageParserT__12)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(54)
			p.Identifier()
		}

		p.SetState(59)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IPkgContext is an interface to support dynamic dispatch.
type IPkgContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	AllIdentifier() []IIdentifierContext
	Identifier(i int) IIdentifierContext

	// IsPkgContext differentiates from other interfaces.
	IsPkgContext()
}

type PkgContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyPkgContext() *PkgContext {
	var p = new(PkgContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_pkg
	return p
}

func InitEmptyPkgContext(p *PkgContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_pkg
}

func (*PkgContext) IsPkgContext() {}

func NewPkgContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *PkgContext {
	var p = new(PkgContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_pkg

	return p
}

func (s *PkgContext) GetParser() antlr.Parser { return s.parser }

func (s *PkgContext) AllIdentifier() []IIdentifierContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IIdentifierContext); ok {
			len++
		}
	}

	tst := make([]IIdentifierContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IIdentifierContext); ok {
			tst[i] = t.(IIdentifierContext)
			i++
		}
	}

	return tst
}

func (s *PkgContext) Identifier(i int) IIdentifierContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *PkgContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *PkgContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *PkgContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitPkg(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Pkg() (localctx IPkgContext) {
	localctx = NewPkgContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 4, StowageParserRULE_pkg)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(60)
		p.Identifier()
	}
	p.SetState(65)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserT__13 || _la == StowageParserT__14 {
		{
			p.SetState(61)
			_la = p.GetTokenStream().LA(1)

			if !(_la == StowageParserT__13 || _la == StowageParserT__14) {
				p.GetErrorHandler().RecoverInline(p)
			} else {
				p.GetErrorHandler().ReportMatch(p)
				p.Consume()
			}
		}
		{
			p.SetState(62)
			p.Identifier()
		}

		p.SetState(67)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IRootContext is an interface to support dynamic dispatch.
type IRootContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	EOF() antlr.TerminalNode
	AllEntity() []IEntityContext
	Entity(i int) IEntityContext
	AllView() []IViewContext
	View(i int) IViewContext
	AllQuery() []IQueryContext
	Query(i int) IQueryContext
	AllEnum() []IEnumContext
	Enum(i int) IEnumContext

	// IsRootContext differentiates from other interfaces.
	IsRootContext()
}

type RootContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyRootContext() *RootContext {
	var p = new(RootContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_root
	return p
}

func InitEmptyRootContext(p *RootContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_root
}

func (*RootContext) IsRootContext() {}

func NewRootContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *RootContext {
	var p = new(RootContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_root

	return p
}

func (s *RootContext) GetParser() antlr.Parser { return s.parser }

func (s *RootContext) EOF() antlr.TerminalNode {
	return s.GetToken(StowageParserEOF, 0)
}

func (s *RootContext) AllEntity() []IEntityContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEntityContext); ok {
			len++
		}
	}

	tst := make([]IEntityContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEntityContext); ok {
			tst[i] = t.(IEntityContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Entity(i int) IEntityContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEntityContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEntityContext)
}

func (s *RootContext) AllView() []IViewContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IViewContext); ok {
			len++
		}
	}

	tst := make([]IViewContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IViewContext); ok {
			tst[i] = t.(IViewContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) View(i int) IViewContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IViewContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IViewContext)
}

func (s *RootContext) AllQuery() []IQueryContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IQueryContext); ok {
			len++
		}
	}

	tst := make([]IQueryContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IQueryContext); ok {
			tst[i] = t.(IQueryContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Query(i int) IQueryContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IQueryContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IQueryContext)
}

func (s *RootContext) AllEnum() []IEnumContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEnumContext); ok {
			len++
		}
	}

	tst := make([]IEnumContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEnumContext); ok {
			tst[i] = t.(IEnumContext)
			i++
		}
	}

	return tst
}

func (s *RootContext) Enum(i int) IEnumContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumContext)
}

func (s *RootContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *RootContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *RootContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitRoot(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Root() (localctx IRootContext) {
	localctx = NewRootContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 6, StowageParserRULE_root)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(74)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&67109776) != 0 {
		p.SetState(72)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}

		switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 2, p.GetParserRuleContext()) {
		case 1:
			{
				p.SetState(68)
				p.Entity()
			}

		case 2:
			{
				p.SetState(69)
				p.View()
			}

		case 3:
			{
				p.SetState(70)
				p.Query()
			}

		case 4:
			{
				p.SetState(71)
				p.Enum()
			}

		case antlr.ATNInvalidAltNumber:
			goto errorExit
		}

		p.SetState(76)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(77)
		p.Match(StowageParserEOF)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEntityContext is an interface to support dynamic dispatch.
type IEntityContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	Identifiers() IIdentifiersContext
	AllAttribute() []IAttributeContext
	Attribute(i int) IAttributeContext

	// IsEntityContext differentiates from other interfaces.
	IsEntityContext()
}

type EntityContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEntityContext() *EntityContext {
	var p = new(EntityContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_entity
	return p
}

func InitEmptyEntityContext(p *EntityContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_entity
}

func (*EntityContext) IsEntityContext() {}

func NewEntityContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EntityContext {
	var p = new(EntityContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_entity

	return p
}

func (s *EntityContext) GetParser() antlr.Parser { return s.parser }

func (s *EntityContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EntityContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(StowageParserCOMMENT)
}

func (s *EntityContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(StowageParserCOMMENT, i)
}

func (s *EntityContext) Identifiers() IIdentifiersContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifiersContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifiersContext)
}

func (s *EntityContext) AllAttribute() []IAttributeContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IAttributeContext); ok {
			len++
		}
	}

	tst := make([]IAttributeContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IAttributeContext); ok {
			tst[i] = t.(IAttributeContext)
			i++
		}
	}

	return tst
}

func (s *EntityContext) Attribute(i int) IAttributeContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IAttributeContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IAttributeContext)
}

func (s *EntityContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EntityContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EntityContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitEntity(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Entity() (localctx IEntityContext) {
	localctx = NewEntityContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 8, StowageParserRULE_entity)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(82)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserCOMMENT {
		{
			p.SetState(79)
			p.Match(StowageParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(84)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(85)
		p.Match(StowageParserT__6)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(86)
		p.Identifier()
	}
	p.SetState(90)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == StowageParserT__9 {
		{
			p.SetState(87)
			p.Match(StowageParserT__9)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(88)
			p.Match(StowageParserT__10)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(89)
			p.Identifiers()
		}

	}
	{
		p.SetState(92)
		p.Match(StowageParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(96)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&17045659646) != 0 {
		{
			p.SetState(93)
			p.Attribute()
		}

		p.SetState(98)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(99)
		p.Match(StowageParserT__16)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IViewContext is an interface to support dynamic dispatch.
type IViewContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	AllViewAttribute() []IViewAttributeContext
	ViewAttribute(i int) IViewAttributeContext

	// IsViewContext differentiates from other interfaces.
	IsViewContext()
}

type ViewContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyViewContext() *ViewContext {
	var p = new(ViewContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_view
	return p
}

func InitEmptyViewContext(p *ViewContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_view
}

func (*ViewContext) IsViewContext() {}

func NewViewContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewContext {
	var p = new(ViewContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_view

	return p
}

func (s *ViewContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ViewContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(StowageParserCOMMENT)
}

func (s *ViewContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(StowageParserCOMMENT, i)
}

func (s *ViewContext) AllViewAttribute() []IViewAttributeContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IViewAttributeContext); ok {
			len++
		}
	}

	tst := make([]IViewAttributeContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IViewAttributeContext); ok {
			tst[i] = t.(IViewAttributeContext)
			i++
		}
	}

	return tst
}

func (s *ViewContext) ViewAttribute(i int) IViewAttributeContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IViewAttributeContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IViewAttributeContext)
}

func (s *ViewContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitView(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) View() (localctx IViewContext) {
	localctx = NewViewContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 10, StowageParserRULE_view)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(104)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserCOMMENT {
		{
			p.SetState(101)
			p.Match(StowageParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(106)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(107)
		p.Match(StowageParserT__7)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(108)
		p.Identifier()
	}
	{
		p.SetState(109)
		p.Match(StowageParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(113)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&17045659646) != 0 {
		{
			p.SetState(110)
			p.ViewAttribute()
		}

		p.SetState(115)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(116)
		p.Match(StowageParserT__16)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IQueryContext is an interface to support dynamic dispatch.
type IQueryContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode
	AllViewAttribute() []IViewAttributeContext
	ViewAttribute(i int) IViewAttributeContext

	// IsQueryContext differentiates from other interfaces.
	IsQueryContext()
}

type QueryContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyQueryContext() *QueryContext {
	var p = new(QueryContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_query
	return p
}

func InitEmptyQueryContext(p *QueryContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_query
}

func (*QueryContext) IsQueryContext() {}

func NewQueryContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *QueryContext {
	var p = new(QueryContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_query

	return p
}

func (s *QueryContext) GetParser() antlr.Parser { return s.parser }

func (s *QueryContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *QueryContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(StowageParserCOMMENT)
}

func (s *QueryContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(StowageParserCOMMENT, i)
}

func (s *QueryContext) AllViewAttribute() []IViewAttributeContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IViewAttributeContext); ok {
			len++
		}
	}

	tst := make([]IViewAttributeContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IViewAttributeContext); ok {
			tst[i] = t.(IViewAttributeContext)
			i++
		}
	}

	return tst
}

func (s *QueryContext) ViewAttribute(i int) IViewAttributeContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IViewAttributeContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IViewAttributeContext)
}

func (s *QueryContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *QueryContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *QueryContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitQuery(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Query() (localctx IQueryContext) {
	localctx = NewQueryContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 12, StowageParserRULE_query)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(121)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserCOMMENT {
		{
			p.SetState(118)
			p.Match(StowageParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(123)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(124)
		p.Match(StowageParserT__8)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(125)
		p.Identifier()
	}
	{
		p.SetState(126)
		p.Match(StowageParserT__15)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	p.SetState(130)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for (int64(_la) & ^0x3f) == 0 && ((int64(1)<<_la)&17045659646) != 0 {
		{
			p.SetState(127)
			p.ViewAttribute()
		}

		p.SetState(132)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(133)
		p.Match(StowageParserT__16)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEnumContext is an interface to support dynamic dispatch.
type IEnumContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	AllEnumValue() []IEnumValueContext
	EnumValue(i int) IEnumValueContext
	AllCOMMENT() []antlr.TerminalNode
	COMMENT(i int) antlr.TerminalNode

	// IsEnumContext differentiates from other interfaces.
	IsEnumContext()
}

type EnumContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumContext() *EnumContext {
	var p = new(EnumContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enum
	return p
}

func InitEmptyEnumContext(p *EnumContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enum
}

func (*EnumContext) IsEnumContext() {}

func NewEnumContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumContext {
	var p = new(EnumContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_enum

	return p
}

func (s *EnumContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EnumContext) AllEnumValue() []IEnumValueContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IEnumValueContext); ok {
			len++
		}
	}

	tst := make([]IEnumValueContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IEnumValueContext); ok {
			tst[i] = t.(IEnumValueContext)
			i++
		}
	}

	return tst
}

func (s *EnumContext) EnumValue(i int) IEnumValueContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumValueContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumValueContext)
}

func (s *EnumContext) AllCOMMENT() []antlr.TerminalNode {
	return s.GetTokens(StowageParserCOMMENT)
}

func (s *EnumContext) COMMENT(i int) antlr.TerminalNode {
	return s.GetToken(StowageParserCOMMENT, i)
}

func (s *EnumContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitEnum(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Enum() (localctx IEnumContext) {
	localctx = NewEnumContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 14, StowageParserRULE_enum)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(138)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for _la == StowageParserCOMMENT {
		{
			p.SetState(135)
			p.Match(StowageParserCOMMENT)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

		p.SetState(140)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(141)
		p.Match(StowageParserT__3)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(142)
		p.Identifier()
	}
	{
		p.SetState(143)
		p.Match(StowageParserT__17)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(144)
		p.EnumValue()
	}
	p.SetState(147)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	for ok := true; ok; ok = _la == StowageParserT__12 {
		{
			p.SetState(145)
			p.Match(StowageParserT__12)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(146)
			p.EnumValue()
		}

		p.SetState(149)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)
	}
	{
		p.SetState(151)
		p.Match(StowageParserT__18)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEnumValueContext is an interface to support dynamic dispatch.
type IEnumValueContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	STRING() antlr.TerminalNode

	// IsEnumValueContext differentiates from other interfaces.
	IsEnumValueContext()
}

type EnumValueContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumValueContext() *EnumValueContext {
	var p = new(EnumValueContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enumValue
	return p
}

func InitEmptyEnumValueContext(p *EnumValueContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enumValue
}

func (*EnumValueContext) IsEnumValueContext() {}

func NewEnumValueContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumValueContext {
	var p = new(EnumValueContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_enumValue

	return p
}

func (s *EnumValueContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumValueContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EnumValueContext) STRING() antlr.TerminalNode {
	return s.GetToken(StowageParserSTRING, 0)
}

func (s *EnumValueContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumValueContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumValueContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitEnumValue(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) EnumValue() (localctx IEnumValueContext) {
	localctx = NewEnumValueContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 16, StowageParserRULE_enumValue)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(153)
		p.Identifier()
	}
	p.SetState(156)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_la = p.GetTokenStream().LA(1)

	if _la == StowageParserT__19 {
		{
			p.SetState(154)
			p.Match(StowageParserT__19)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(155)
			p.Match(StowageParserSTRING)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IAttributeContext is an interface to support dynamic dispatch.
type IAttributeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	UniqueTypeDef() IUniqueTypeDefContext
	TypeDef() ITypeDefContext
	ComputedAttribute() IComputedAttributeContext
	UNIQUE() antlr.TerminalNode

	// IsAttributeContext differentiates from other interfaces.
	IsAttributeContext()
}

type AttributeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyAttributeContext() *AttributeContext {
	var p = new(AttributeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_attribute
	return p
}

func InitEmptyAttributeContext(p *AttributeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_attribute
}

func (*AttributeContext) IsAttributeContext() {}

func NewAttributeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *AttributeContext {
	var p = new(AttributeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_attribute

	return p
}

func (s *AttributeContext) GetParser() antlr.Parser { return s.parser }

func (s *AttributeContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *AttributeContext) UniqueTypeDef() IUniqueTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IUniqueTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IUniqueTypeDefContext)
}

func (s *AttributeContext) TypeDef() ITypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITypeDefContext)
}

func (s *AttributeContext) ComputedAttribute() IComputedAttributeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IComputedAttributeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IComputedAttributeContext)
}

func (s *AttributeContext) UNIQUE() antlr.TerminalNode {
	return s.GetToken(StowageParserUNIQUE, 0)
}

func (s *AttributeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *AttributeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *AttributeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitAttribute(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Attribute() (localctx IAttributeContext) {
	localctx = NewAttributeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 18, StowageParserRULE_attribute)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(158)
		p.Identifier()
	}
	p.SetState(165)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case StowageParserT__0, StowageParserT__1, StowageParserT__2, StowageParserT__5, StowageParserUNIQUE:
		p.SetState(160)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_la = p.GetTokenStream().LA(1)

		if _la == StowageParserUNIQUE {
			{
				p.SetState(159)
				p.Match(StowageParserUNIQUE)
				if p.HasError() {
					// Recognition error - abort rule
					goto errorExit
				}
			}

		}
		{
			p.SetState(162)
			p.UniqueTypeDef()
		}

	case StowageParserT__3, StowageParserT__4, StowageParserT__20, StowageParserT__21:
		{
			p.SetState(163)
			p.TypeDef()
		}

	case StowageParserCOMPUTED:
		{
			p.SetState(164)
			p.ComputedAttribute()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IViewAttributeContext is an interface to support dynamic dispatch.
type IViewAttributeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext
	ViewTypeDef() IViewTypeDefContext

	// IsViewAttributeContext differentiates from other interfaces.
	IsViewAttributeContext()
}

type ViewAttributeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyViewAttributeContext() *ViewAttributeContext {
	var p = new(ViewAttributeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_viewAttribute
	return p
}

func InitEmptyViewAttributeContext(p *ViewAttributeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_viewAttribute
}

func (*ViewAttributeContext) IsViewAttributeContext() {}

func NewViewAttributeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewAttributeContext {
	var p = new(ViewAttributeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_viewAttribute

	return p
}

func (s *ViewAttributeContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewAttributeContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ViewAttributeContext) ViewTypeDef() IViewTypeDefContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IViewTypeDefContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IViewTypeDefContext)
}

func (s *ViewAttributeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewAttributeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewAttributeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitViewAttribute(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) ViewAttribute() (localctx IViewAttributeContext) {
	localctx = NewViewAttributeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 20, StowageParserRULE_viewAttribute)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(167)
		p.Identifier()
	}

	{
		p.SetState(168)
		p.ViewTypeDef()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IUniqueTypeDefContext is an interface to support dynamic dispatch.
type IUniqueTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IntType() IIntTypeContext
	FloatType() IFloatTypeContext
	StringType() IStringTypeContext
	TimeType() ITimeTypeContext

	// IsUniqueTypeDefContext differentiates from other interfaces.
	IsUniqueTypeDefContext()
}

type UniqueTypeDefContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyUniqueTypeDefContext() *UniqueTypeDefContext {
	var p = new(UniqueTypeDefContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_uniqueTypeDef
	return p
}

func InitEmptyUniqueTypeDefContext(p *UniqueTypeDefContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_uniqueTypeDef
}

func (*UniqueTypeDefContext) IsUniqueTypeDefContext() {}

func NewUniqueTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *UniqueTypeDefContext {
	var p = new(UniqueTypeDefContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_uniqueTypeDef

	return p
}

func (s *UniqueTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *UniqueTypeDefContext) IntType() IIntTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIntTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIntTypeContext)
}

func (s *UniqueTypeDefContext) FloatType() IFloatTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFloatTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFloatTypeContext)
}

func (s *UniqueTypeDefContext) StringType() IStringTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStringTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStringTypeContext)
}

func (s *UniqueTypeDefContext) TimeType() ITimeTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITimeTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITimeTypeContext)
}

func (s *UniqueTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *UniqueTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *UniqueTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitUniqueTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) UniqueTypeDef() (localctx IUniqueTypeDefContext) {
	localctx = NewUniqueTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 22, StowageParserRULE_uniqueTypeDef)
	p.SetState(174)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case StowageParserT__0:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(170)
			p.IntType()
		}

	case StowageParserT__1:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(171)
			p.FloatType()
		}

	case StowageParserT__2:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(172)
			p.StringType()
		}

	case StowageParserT__5:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(173)
			p.TimeType()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITypeDefContext is an interface to support dynamic dispatch.
type ITypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	SerialType() ISerialTypeContext
	BoolType() IBoolTypeContext
	EnumType() IEnumTypeContext
	ReferenceType() IReferenceTypeContext

	// IsTypeDefContext differentiates from other interfaces.
	IsTypeDefContext()
}

type TypeDefContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTypeDefContext() *TypeDefContext {
	var p = new(TypeDefContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_typeDef
	return p
}

func InitEmptyTypeDefContext(p *TypeDefContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_typeDef
}

func (*TypeDefContext) IsTypeDefContext() {}

func NewTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TypeDefContext {
	var p = new(TypeDefContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_typeDef

	return p
}

func (s *TypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *TypeDefContext) SerialType() ISerialTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ISerialTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ISerialTypeContext)
}

func (s *TypeDefContext) BoolType() IBoolTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IBoolTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IBoolTypeContext)
}

func (s *TypeDefContext) EnumType() IEnumTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumTypeContext)
}

func (s *TypeDefContext) ReferenceType() IReferenceTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IReferenceTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IReferenceTypeContext)
}

func (s *TypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) TypeDef() (localctx ITypeDefContext) {
	localctx = NewTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 24, StowageParserRULE_typeDef)
	p.SetState(180)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case StowageParserT__20:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(176)
			p.SerialType()
		}

	case StowageParserT__4:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(177)
			p.BoolType()
		}

	case StowageParserT__3:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(178)
			p.EnumType()
		}

	case StowageParserT__21:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(179)
			p.ReferenceType()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IViewTypeDefContext is an interface to support dynamic dispatch.
type IViewTypeDefContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	IntType() IIntTypeContext
	FloatType() IFloatTypeContext
	StringType() IStringTypeContext
	TimeType() ITimeTypeContext
	BoolType() IBoolTypeContext
	EnumType() IEnumTypeContext
	ReferenceType() IReferenceTypeContext

	// IsViewTypeDefContext differentiates from other interfaces.
	IsViewTypeDefContext()
}

type ViewTypeDefContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyViewTypeDefContext() *ViewTypeDefContext {
	var p = new(ViewTypeDefContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_viewTypeDef
	return p
}

func InitEmptyViewTypeDefContext(p *ViewTypeDefContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_viewTypeDef
}

func (*ViewTypeDefContext) IsViewTypeDefContext() {}

func NewViewTypeDefContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ViewTypeDefContext {
	var p = new(ViewTypeDefContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_viewTypeDef

	return p
}

func (s *ViewTypeDefContext) GetParser() antlr.Parser { return s.parser }

func (s *ViewTypeDefContext) IntType() IIntTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIntTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIntTypeContext)
}

func (s *ViewTypeDefContext) FloatType() IFloatTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IFloatTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IFloatTypeContext)
}

func (s *ViewTypeDefContext) StringType() IStringTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IStringTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IStringTypeContext)
}

func (s *ViewTypeDefContext) TimeType() ITimeTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ITimeTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ITimeTypeContext)
}

func (s *ViewTypeDefContext) BoolType() IBoolTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IBoolTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IBoolTypeContext)
}

func (s *ViewTypeDefContext) EnumType() IEnumTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IEnumTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IEnumTypeContext)
}

func (s *ViewTypeDefContext) ReferenceType() IReferenceTypeContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IReferenceTypeContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IReferenceTypeContext)
}

func (s *ViewTypeDefContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ViewTypeDefContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ViewTypeDefContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitViewTypeDef(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) ViewTypeDef() (localctx IViewTypeDefContext) {
	localctx = NewViewTypeDefContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 26, StowageParserRULE_viewTypeDef)
	p.SetState(189)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetTokenStream().LA(1) {
	case StowageParserT__0:
		p.EnterOuterAlt(localctx, 1)
		{
			p.SetState(182)
			p.IntType()
		}

	case StowageParserT__1:
		p.EnterOuterAlt(localctx, 2)
		{
			p.SetState(183)
			p.FloatType()
		}

	case StowageParserT__2:
		p.EnterOuterAlt(localctx, 3)
		{
			p.SetState(184)
			p.StringType()
		}

	case StowageParserT__5:
		p.EnterOuterAlt(localctx, 4)
		{
			p.SetState(185)
			p.TimeType()
		}

	case StowageParserT__4:
		p.EnterOuterAlt(localctx, 5)
		{
			p.SetState(186)
			p.BoolType()
		}

	case StowageParserT__3:
		p.EnterOuterAlt(localctx, 6)
		{
			p.SetState(187)
			p.EnumType()
		}

	case StowageParserT__21:
		p.EnterOuterAlt(localctx, 7)
		{
			p.SetState(188)
			p.ReferenceType()
		}

	default:
		p.SetError(antlr.NewNoViableAltException(p, nil, nil, nil, nil, nil))
		goto errorExit
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IComputedAttributeContext is an interface to support dynamic dispatch.
type IComputedAttributeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	COMPUTED() antlr.TerminalNode
	Expression() IExpressionContext

	// IsComputedAttributeContext differentiates from other interfaces.
	IsComputedAttributeContext()
}

type ComputedAttributeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyComputedAttributeContext() *ComputedAttributeContext {
	var p = new(ComputedAttributeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_computedAttribute
	return p
}

func InitEmptyComputedAttributeContext(p *ComputedAttributeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_computedAttribute
}

func (*ComputedAttributeContext) IsComputedAttributeContext() {}

func NewComputedAttributeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ComputedAttributeContext {
	var p = new(ComputedAttributeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_computedAttribute

	return p
}

func (s *ComputedAttributeContext) GetParser() antlr.Parser { return s.parser }

func (s *ComputedAttributeContext) COMPUTED() antlr.TerminalNode {
	return s.GetToken(StowageParserCOMPUTED, 0)
}

func (s *ComputedAttributeContext) Expression() IExpressionContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ComputedAttributeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ComputedAttributeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ComputedAttributeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitComputedAttribute(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) ComputedAttribute() (localctx IComputedAttributeContext) {
	localctx = NewComputedAttributeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 28, StowageParserRULE_computedAttribute)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(191)
		p.Match(StowageParserCOMPUTED)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(192)
		p.expression(0)
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IIntTypeContext is an interface to support dynamic dispatch.
type IIntTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsIntTypeContext differentiates from other interfaces.
	IsIntTypeContext()
}

type IntTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyIntTypeContext() *IntTypeContext {
	var p = new(IntTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_intType
	return p
}

func InitEmptyIntTypeContext(p *IntTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_intType
}

func (*IntTypeContext) IsIntTypeContext() {}

func NewIntTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *IntTypeContext {
	var p = new(IntTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_intType

	return p
}

func (s *IntTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *IntTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *IntTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *IntTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitIntType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) IntType() (localctx IIntTypeContext) {
	localctx = NewIntTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 30, StowageParserRULE_intType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(194)
		p.Match(StowageParserT__0)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ISerialTypeContext is an interface to support dynamic dispatch.
type ISerialTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsSerialTypeContext differentiates from other interfaces.
	IsSerialTypeContext()
}

type SerialTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptySerialTypeContext() *SerialTypeContext {
	var p = new(SerialTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_serialType
	return p
}

func InitEmptySerialTypeContext(p *SerialTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_serialType
}

func (*SerialTypeContext) IsSerialTypeContext() {}

func NewSerialTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *SerialTypeContext {
	var p = new(SerialTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_serialType

	return p
}

func (s *SerialTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *SerialTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *SerialTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *SerialTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitSerialType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) SerialType() (localctx ISerialTypeContext) {
	localctx = NewSerialTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 32, StowageParserRULE_serialType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(196)
		p.Match(StowageParserT__20)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IFloatTypeContext is an interface to support dynamic dispatch.
type IFloatTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsFloatTypeContext differentiates from other interfaces.
	IsFloatTypeContext()
}

type FloatTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyFloatTypeContext() *FloatTypeContext {
	var p = new(FloatTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_floatType
	return p
}

func InitEmptyFloatTypeContext(p *FloatTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_floatType
}

func (*FloatTypeContext) IsFloatTypeContext() {}

func NewFloatTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *FloatTypeContext {
	var p = new(FloatTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_floatType

	return p
}

func (s *FloatTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *FloatTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *FloatTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *FloatTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitFloatType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) FloatType() (localctx IFloatTypeContext) {
	localctx = NewFloatTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 34, StowageParserRULE_floatType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(198)
		p.Match(StowageParserT__1)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IStringTypeContext is an interface to support dynamic dispatch.
type IStringTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsStringTypeContext differentiates from other interfaces.
	IsStringTypeContext()
}

type StringTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyStringTypeContext() *StringTypeContext {
	var p = new(StringTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_stringType
	return p
}

func InitEmptyStringTypeContext(p *StringTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_stringType
}

func (*StringTypeContext) IsStringTypeContext() {}

func NewStringTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *StringTypeContext {
	var p = new(StringTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_stringType

	return p
}

func (s *StringTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *StringTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *StringTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *StringTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitStringType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) StringType() (localctx IStringTypeContext) {
	localctx = NewStringTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 36, StowageParserRULE_stringType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(200)
		p.Match(StowageParserT__2)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IBoolTypeContext is an interface to support dynamic dispatch.
type IBoolTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsBoolTypeContext differentiates from other interfaces.
	IsBoolTypeContext()
}

type BoolTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyBoolTypeContext() *BoolTypeContext {
	var p = new(BoolTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_boolType
	return p
}

func InitEmptyBoolTypeContext(p *BoolTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_boolType
}

func (*BoolTypeContext) IsBoolTypeContext() {}

func NewBoolTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *BoolTypeContext {
	var p = new(BoolTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_boolType

	return p
}

func (s *BoolTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *BoolTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *BoolTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *BoolTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitBoolType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) BoolType() (localctx IBoolTypeContext) {
	localctx = NewBoolTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 38, StowageParserRULE_boolType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(202)
		p.Match(StowageParserT__4)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IEnumTypeContext is an interface to support dynamic dispatch.
type IEnumTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext

	// IsEnumTypeContext differentiates from other interfaces.
	IsEnumTypeContext()
}

type EnumTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyEnumTypeContext() *EnumTypeContext {
	var p = new(EnumTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enumType
	return p
}

func InitEmptyEnumTypeContext(p *EnumTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_enumType
}

func (*EnumTypeContext) IsEnumTypeContext() {}

func NewEnumTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *EnumTypeContext {
	var p = new(EnumTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_enumType

	return p
}

func (s *EnumTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *EnumTypeContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *EnumTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *EnumTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *EnumTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitEnumType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) EnumType() (localctx IEnumTypeContext) {
	localctx = NewEnumTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 40, StowageParserRULE_enumType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(204)
		p.Match(StowageParserT__3)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(205)
		p.Identifier()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ITimeTypeContext is an interface to support dynamic dispatch.
type ITimeTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser
	// IsTimeTypeContext differentiates from other interfaces.
	IsTimeTypeContext()
}

type TimeTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyTimeTypeContext() *TimeTypeContext {
	var p = new(TimeTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_timeType
	return p
}

func InitEmptyTimeTypeContext(p *TimeTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_timeType
}

func (*TimeTypeContext) IsTimeTypeContext() {}

func NewTimeTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *TimeTypeContext {
	var p = new(TimeTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_timeType

	return p
}

func (s *TimeTypeContext) GetParser() antlr.Parser { return s.parser }
func (s *TimeTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *TimeTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *TimeTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitTimeType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) TimeType() (localctx ITimeTypeContext) {
	localctx = NewTimeTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 42, StowageParserRULE_timeType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(207)
		p.Match(StowageParserT__5)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IReferenceTypeContext is an interface to support dynamic dispatch.
type IReferenceTypeContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	Identifier() IIdentifierContext

	// IsReferenceTypeContext differentiates from other interfaces.
	IsReferenceTypeContext()
}

type ReferenceTypeContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyReferenceTypeContext() *ReferenceTypeContext {
	var p = new(ReferenceTypeContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_referenceType
	return p
}

func InitEmptyReferenceTypeContext(p *ReferenceTypeContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_referenceType
}

func (*ReferenceTypeContext) IsReferenceTypeContext() {}

func NewReferenceTypeContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ReferenceTypeContext {
	var p = new(ReferenceTypeContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_referenceType

	return p
}

func (s *ReferenceTypeContext) GetParser() antlr.Parser { return s.parser }

func (s *ReferenceTypeContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ReferenceTypeContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ReferenceTypeContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ReferenceTypeContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitReferenceType(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) ReferenceType() (localctx IReferenceTypeContext) {
	localctx = NewReferenceTypeContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 44, StowageParserRULE_referenceType)
	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(209)
		p.Match(StowageParserT__21)
		if p.HasError() {
			// Recognition error - abort rule
			goto errorExit
		}
	}
	{
		p.SetState(210)
		p.Identifier()
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// IExpressionContext is an interface to support dynamic dispatch.
type IExpressionContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// GetHash returns the hash token.
	GetHash() antlr.Token

	// GetAbs returns the abs token.
	GetAbs() antlr.Token

	// GetLower returns the lower token.
	GetLower() antlr.Token

	// GetUpper returns the upper token.
	GetUpper() antlr.Token

	// GetAddition returns the addition token.
	GetAddition() antlr.Token

	// GetModulo returns the modulo token.
	GetModulo() antlr.Token

	// SetHash sets the hash token.
	SetHash(antlr.Token)

	// SetAbs sets the abs token.
	SetAbs(antlr.Token)

	// SetLower sets the lower token.
	SetLower(antlr.Token)

	// SetUpper sets the upper token.
	SetUpper(antlr.Token)

	// SetAddition sets the addition token.
	SetAddition(antlr.Token)

	// SetModulo sets the modulo token.
	SetModulo(antlr.Token)

	// GetColumn returns the column rule contexts.
	GetColumn() IIdentifierContext

	// SetColumn sets the column rule contexts.
	SetColumn(IIdentifierContext)

	// Getter signatures
	Literal() ILiteralContext
	AllExpression() []IExpressionContext
	Expression(i int) IExpressionContext
	HASH() antlr.TerminalNode
	ABS() antlr.TerminalNode
	LOWER() antlr.TerminalNode
	UPPER() antlr.TerminalNode
	Identifier() IIdentifierContext

	// IsExpressionContext differentiates from other interfaces.
	IsExpressionContext()
}

type ExpressionContext struct {
	antlr.BaseParserRuleContext
	parser   antlr.Parser
	hash     antlr.Token
	abs      antlr.Token
	lower    antlr.Token
	upper    antlr.Token
	column   IIdentifierContext
	addition antlr.Token
	modulo   antlr.Token
}

func NewEmptyExpressionContext() *ExpressionContext {
	var p = new(ExpressionContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_expression
	return p
}

func InitEmptyExpressionContext(p *ExpressionContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_expression
}

func (*ExpressionContext) IsExpressionContext() {}

func NewExpressionContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *ExpressionContext {
	var p = new(ExpressionContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_expression

	return p
}

func (s *ExpressionContext) GetParser() antlr.Parser { return s.parser }

func (s *ExpressionContext) GetHash() antlr.Token { return s.hash }

func (s *ExpressionContext) GetAbs() antlr.Token { return s.abs }

func (s *ExpressionContext) GetLower() antlr.Token { return s.lower }

func (s *ExpressionContext) GetUpper() antlr.Token { return s.upper }

func (s *ExpressionContext) GetAddition() antlr.Token { return s.addition }

func (s *ExpressionContext) GetModulo() antlr.Token { return s.modulo }

func (s *ExpressionContext) SetHash(v antlr.Token) { s.hash = v }

func (s *ExpressionContext) SetAbs(v antlr.Token) { s.abs = v }

func (s *ExpressionContext) SetLower(v antlr.Token) { s.lower = v }

func (s *ExpressionContext) SetUpper(v antlr.Token) { s.upper = v }

func (s *ExpressionContext) SetAddition(v antlr.Token) { s.addition = v }

func (s *ExpressionContext) SetModulo(v antlr.Token) { s.modulo = v }

func (s *ExpressionContext) GetColumn() IIdentifierContext { return s.column }

func (s *ExpressionContext) SetColumn(v IIdentifierContext) { s.column = v }

func (s *ExpressionContext) Literal() ILiteralContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(ILiteralContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(ILiteralContext)
}

func (s *ExpressionContext) AllExpression() []IExpressionContext {
	children := s.GetChildren()
	len := 0
	for _, ctx := range children {
		if _, ok := ctx.(IExpressionContext); ok {
			len++
		}
	}

	tst := make([]IExpressionContext, len)
	i := 0
	for _, ctx := range children {
		if t, ok := ctx.(IExpressionContext); ok {
			tst[i] = t.(IExpressionContext)
			i++
		}
	}

	return tst
}

func (s *ExpressionContext) Expression(i int) IExpressionContext {
	var t antlr.RuleContext
	j := 0
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IExpressionContext); ok {
			if j == i {
				t = ctx.(antlr.RuleContext)
				break
			}
			j++
		}
	}

	if t == nil {
		return nil
	}

	return t.(IExpressionContext)
}

func (s *ExpressionContext) HASH() antlr.TerminalNode {
	return s.GetToken(StowageParserHASH, 0)
}

func (s *ExpressionContext) ABS() antlr.TerminalNode {
	return s.GetToken(StowageParserABS, 0)
}

func (s *ExpressionContext) LOWER() antlr.TerminalNode {
	return s.GetToken(StowageParserLOWER, 0)
}

func (s *ExpressionContext) UPPER() antlr.TerminalNode {
	return s.GetToken(StowageParserUPPER, 0)
}

func (s *ExpressionContext) Identifier() IIdentifierContext {
	var t antlr.RuleContext
	for _, ctx := range s.GetChildren() {
		if _, ok := ctx.(IIdentifierContext); ok {
			t = ctx.(antlr.RuleContext)
			break
		}
	}

	if t == nil {
		return nil
	}

	return t.(IIdentifierContext)
}

func (s *ExpressionContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *ExpressionContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *ExpressionContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitExpression(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Expression() (localctx IExpressionContext) {
	return p.expression(0)
}

func (p *StowageParser) expression(_p int) (localctx IExpressionContext) {
	var _parentctx antlr.ParserRuleContext = p.GetParserRuleContext()

	_parentState := p.GetState()
	localctx = NewExpressionContext(p, p.GetParserRuleContext(), _parentState)
	var _prevctx IExpressionContext = localctx
	var _ antlr.ParserRuleContext = _prevctx // TODO: To prevent unused variable warning.
	_startState := 46
	p.EnterRecursionRule(localctx, 46, StowageParserRULE_expression, _p)
	var _alt int

	p.EnterOuterAlt(localctx, 1)
	p.SetState(235)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}

	switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 19, p.GetParserRuleContext()) {
	case 1:

	case 2:
		{
			p.SetState(213)
			p.Literal()
		}

	case 3:
		{
			p.SetState(214)

			var _m = p.Match(StowageParserHASH)

			localctx.(*ExpressionContext).hash = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(215)
			p.Match(StowageParserT__17)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(216)
			p.expression(0)
		}
		{
			p.SetState(217)
			p.Match(StowageParserT__18)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 4:
		{
			p.SetState(219)

			var _m = p.Match(StowageParserABS)

			localctx.(*ExpressionContext).abs = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(220)
			p.Match(StowageParserT__17)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(221)
			p.expression(0)
		}
		{
			p.SetState(222)
			p.Match(StowageParserT__18)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 5:
		{
			p.SetState(224)

			var _m = p.Match(StowageParserLOWER)

			localctx.(*ExpressionContext).lower = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(225)
			p.Match(StowageParserT__17)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(226)
			p.expression(0)
		}
		{
			p.SetState(227)
			p.Match(StowageParserT__18)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 6:
		{
			p.SetState(229)

			var _m = p.Match(StowageParserUPPER)

			localctx.(*ExpressionContext).upper = _m
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(230)
			p.Match(StowageParserT__17)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}
		{
			p.SetState(231)
			p.expression(0)
		}
		{
			p.SetState(232)
			p.Match(StowageParserT__18)
			if p.HasError() {
				// Recognition error - abort rule
				goto errorExit
			}
		}

	case 7:
		{
			p.SetState(234)

			var _x = p.Identifier()

			localctx.(*ExpressionContext).column = _x
		}

	case antlr.ATNInvalidAltNumber:
		goto errorExit
	}
	p.GetParserRuleContext().SetStop(p.GetTokenStream().LT(-1))
	p.SetState(245)
	p.GetErrorHandler().Sync(p)
	if p.HasError() {
		goto errorExit
	}
	_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 21, p.GetParserRuleContext())
	if p.HasError() {
		goto errorExit
	}
	for _alt != 2 && _alt != antlr.ATNInvalidAltNumber {
		if _alt == 1 {
			if p.GetParseListeners() != nil {
				p.TriggerExitRuleEvent()
			}
			_prevctx = localctx
			p.SetState(243)
			p.GetErrorHandler().Sync(p)
			if p.HasError() {
				goto errorExit
			}

			switch p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 20, p.GetParserRuleContext()) {
			case 1:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, StowageParserRULE_expression)
				p.SetState(237)

				if !(p.Precpred(p.GetParserRuleContext(), 8)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 8)", ""))
					goto errorExit
				}
				{
					p.SetState(238)

					var _m = p.Match(StowageParserT__22)

					localctx.(*ExpressionContext).addition = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(239)
					p.expression(9)
				}

			case 2:
				localctx = NewExpressionContext(p, _parentctx, _parentState)
				p.PushNewRecursionContext(localctx, _startState, StowageParserRULE_expression)
				p.SetState(240)

				if !(p.Precpred(p.GetParserRuleContext(), 7)) {
					p.SetError(antlr.NewFailedPredicateException(p, "p.Precpred(p.GetParserRuleContext(), 7)", ""))
					goto errorExit
				}
				{
					p.SetState(241)

					var _m = p.Match(StowageParserT__23)

					localctx.(*ExpressionContext).modulo = _m
					if p.HasError() {
						// Recognition error - abort rule
						goto errorExit
					}
				}
				{
					p.SetState(242)
					p.expression(8)
				}

			case antlr.ATNInvalidAltNumber:
				goto errorExit
			}

		}
		p.SetState(247)
		p.GetErrorHandler().Sync(p)
		if p.HasError() {
			goto errorExit
		}
		_alt = p.GetInterpreter().AdaptivePredict(p.BaseParser, p.GetTokenStream(), 21, p.GetParserRuleContext())
		if p.HasError() {
			goto errorExit
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.UnrollRecursionContexts(_parentctx)
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

// ILiteralContext is an interface to support dynamic dispatch.
type ILiteralContext interface {
	antlr.ParserRuleContext

	// GetParser returns the parser.
	GetParser() antlr.Parser

	// Getter signatures
	STRING() antlr.TerminalNode
	INT() antlr.TerminalNode

	// IsLiteralContext differentiates from other interfaces.
	IsLiteralContext()
}

type LiteralContext struct {
	antlr.BaseParserRuleContext
	parser antlr.Parser
}

func NewEmptyLiteralContext() *LiteralContext {
	var p = new(LiteralContext)
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_literal
	return p
}

func InitEmptyLiteralContext(p *LiteralContext) {
	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, nil, -1)
	p.RuleIndex = StowageParserRULE_literal
}

func (*LiteralContext) IsLiteralContext() {}

func NewLiteralContext(parser antlr.Parser, parent antlr.ParserRuleContext, invokingState int) *LiteralContext {
	var p = new(LiteralContext)

	antlr.InitBaseParserRuleContext(&p.BaseParserRuleContext, parent, invokingState)

	p.parser = parser
	p.RuleIndex = StowageParserRULE_literal

	return p
}

func (s *LiteralContext) GetParser() antlr.Parser { return s.parser }

func (s *LiteralContext) STRING() antlr.TerminalNode {
	return s.GetToken(StowageParserSTRING, 0)
}

func (s *LiteralContext) INT() antlr.TerminalNode {
	return s.GetToken(StowageParserINT, 0)
}

func (s *LiteralContext) GetRuleContext() antlr.RuleContext {
	return s
}

func (s *LiteralContext) ToStringTree(ruleNames []string, recog antlr.Recognizer) string {
	return antlr.TreesStringTree(s, ruleNames, recog)
}

func (s *LiteralContext) Accept(visitor antlr.ParseTreeVisitor) interface{} {
	switch t := visitor.(type) {
	case StowageVisitor:
		return t.VisitLiteral(s)

	default:
		return t.VisitChildren(s)
	}
}

func (p *StowageParser) Literal() (localctx ILiteralContext) {
	localctx = NewLiteralContext(p, p.GetParserRuleContext(), p.GetState())
	p.EnterRule(localctx, 48, StowageParserRULE_literal)
	var _la int

	p.EnterOuterAlt(localctx, 1)
	{
		p.SetState(248)
		_la = p.GetTokenStream().LA(1)

		if !(_la == StowageParserSTRING || _la == StowageParserINT) {
			p.GetErrorHandler().RecoverInline(p)
		} else {
			p.GetErrorHandler().ReportMatch(p)
			p.Consume()
		}
	}

errorExit:
	if p.HasError() {
		v := p.GetError()
		localctx.SetException(v)
		p.GetErrorHandler().ReportError(p, v)
		p.GetErrorHandler().Recover(p, v)
		p.SetError(nil)
	}
	p.ExitRule()
	return localctx
	goto errorExit // Trick to prevent compiler error if the label is not used
}

func (p *StowageParser) Sempred(localctx antlr.RuleContext, ruleIndex, predIndex int) bool {
	switch ruleIndex {
	case 23:
		var t *ExpressionContext = nil
		if localctx != nil {
			t = localctx.(*ExpressionContext)
		}
		return p.Expression_Sempred(t, predIndex)

	default:
		panic("No predicate with index: " + fmt.Sprint(ruleIndex))
	}
}

func (p *StowageParser) Expression_Sempred(localctx antlr.RuleContext, predIndex int) bool {
	switch predIndex {
	case 0:
		return p.Precpred(p.GetParserRuleContext(), 8)

	case 1:
		return p.Precpred(p.GetParserRuleContext(), 7)

	default:
		panic("No predicate with index: " + fmt.Sprint(predIndex))
	}
}
