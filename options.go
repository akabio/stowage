package stowage

type Option interface {
	UpdateConfig(config) config
}

type nameFormat struct {
	format string
}

type replaceStringOption struct {
	from string
	to   string
}

type customPluralOption struct {
	singular string
	plural   string
}

type uniqueNamePrefixOption struct {
	prefix string
}
type preCreateSQLOption struct {
	value string
}
type postCreateSQLOption struct {
	value string
}
type preDropSQLOption struct {
	value string
}
type postDropSQLOption struct {
	value string
}

type config struct {
	PreCreateSQL  string
	PostCreateSQL string
	PreDropSQL    string
	PostDropSQL   string

	NameFormat       string
	UniqueNamePrefix string
	replaceMap       map[string]string
	customPlural     map[string]string
}

// OptionUniqueNamePrefix sets the prefix that is
// used when table/column names colide with keywords
// for example user will be renamd to db_user.
// The default prefix is "db_".
// The prefix is only used for SQL the client code uses
// the unprefixed name.
func OptionUniqueNamePrefix(prefix string) Option {
	return &uniqueNamePrefixOption{prefix: prefix}
}

// OptionNameFormat sets how names are created from
// multiple elements. Valid values are:
//
//	as-id, as_id, AS_ID, asId, AsId, asID, AsID, asid, ASID
//
// default is as_id which for example produces user_group_id
// as sql name.
func OptionNameFormat(format string) Option {
	return &nameFormat{format: format}
}

func OptionReplaceName(from string, to string) Option {
	return &replaceStringOption{from: from, to: to}
}

// OptionCustomPlural sets the plural nome for a custom word.
// For Example OptionCustomPlural("data", "datas")
// Will produce
func OptionCustomPlural(singular, plural string) Option {
	return &customPluralOption{singular: singular, plural: plural}
}

func OptionPreCreateSQL(sql string) Option {
	return &preCreateSQLOption{
		value: sql,
	}
}

func OptionPostCreateSQL(sql string) Option {
	return &postCreateSQLOption{
		value: sql,
	}
}

func OptionPreDropSQL(sql string) Option {
	return &preDropSQLOption{
		value: sql,
	}
}

func OptionPostDropSQL(sql string) Option {
	return &postDropSQLOption{
		value: sql,
	}
}

func (o nameFormat) UpdateConfig(c config) config {
	c.NameFormat = o.format
	return c
}

func (o replaceStringOption) UpdateConfig(c config) config {
	c.replaceMap[o.from] = o.to
	return c
}

func (o customPluralOption) UpdateConfig(c config) config {
	c.customPlural[o.singular] = o.plural
	return c
}

func (o uniqueNamePrefixOption) UpdateConfig(c config) config {
	c.UniqueNamePrefix = o.prefix
	return c
}

func (o preCreateSQLOption) UpdateConfig(c config) config {
	c.PreCreateSQL = o.value
	return c
}

func (o postCreateSQLOption) UpdateConfig(c config) config {
	c.PostCreateSQL = o.value
	return c
}

func (o preDropSQLOption) UpdateConfig(c config) config {
	c.PreDropSQL = o.value
	return c
}

func (o postDropSQLOption) UpdateConfig(c config) config {
	c.PostDropSQL = o.value
	return c
}
