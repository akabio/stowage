package stowage

import (
	"log"
	"os"
	"path/filepath"

	"gitlab.com/akabio/fmtid"
	"gitlab.com/akabio/gogen"
	"gitlab.com/akabio/gopath"
	"gitlab.com/akabio/stowage/ast"
	"gitlab.com/akabio/stowage/generate"
	"gitlab.com/akabio/stowage/parser"

	// we need to include the dependencies the generated code
	// uses so it's available form where this is imported in the
	// version defined here.
	_ "gitlab.com/akabio/mksql"
)

type Generator struct {
	ast           *ast.AST
	interfacePkg  string
	interfacePath string
	filters       []gogen.Option
}

// Generate accepts only the option OptionCustomPlural() so far.
func Generate(source []byte, sourceFolder string, path string, options ...Option) (*Generator, error) {
	ast, err := parser.Parse(string(source), sourceFolder)
	if err != nil {
		return nil, err
	}

	customFormatDec := &fmtid.Formatter{
		Acronyms:       fmtid.ASCII.Acronyms,
		PluralAcronyms: fmtid.ASCII.PluralAcronyms,
		IsFirst:        fmtid.ASCII.IsFirst,
		IsOther:        fmtid.ASCII.IsOther,
		Plurals:        map[string]string{},
	}

	for _, opt := range options {
		plural, isPlural := opt.(*customPluralOption)
		if isPlural {
			customFormatDec.Plurals[plural.singular] = plural.plural
		}
	}

	customFormat := fmtid.AddDefault(customFormatDec)

	filters := []gogen.Option{}
	for k, fn := range customFormat.Formats {
		filters = append(filters, gogen.FilterOption(k, fn))
	}

	filters = append(filters, gogen.FilterOption("noVirtual", generate.NoVirtual))

	err = os.MkdirAll(path, 0o755)
	if err != nil {
		return nil, err
	}

	interfacePkg := filepath.Base(path)
	interfacePath, err := gopath.Find(path)
	if err != nil {
		log.Fatalf("could not find pkg path, %v", err)
	}
	vars := map[string]interface{}{
		"ipkg": interfacePkg,
	}

	for _, ent := range ast.Entities {
		ifName, err := fmtid.ASCII.Fmt("AsID", ent.Name)
		if err != nil {
			log.Fatal(err)
		}

		exists, err := interfaceExists(path, ifName+"Extension")
		if err != nil {
			log.Fatal(err)
		}

		if exists {
			ent.Extension = true
		}
	}

	err = generate.Generate(ast, filepath.Join(path, "interface.gen.go"), "iface/interface.gg", vars, filters)
	if err != nil {
		return nil, err
	}
	err = generate.Generate(ast, filepath.Join(path, "dao.gen.go"), "iface/dao.gg", vars, filters)
	if err != nil {
		return nil, err
	}
	err = generate.Generate(ast, filepath.Join(path, "errors.gen.go"), "iface/errors.gg", vars, filters)
	if err != nil {
		return nil, err
	}

	return &Generator{
		ast:           ast,
		interfacePkg:  interfacePkg,
		interfacePath: interfacePath,
		filters:       filters,
	}, nil
}

func (g *Generator) Test(path string) error {
	err := os.MkdirAll(path, 0o755)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg":  g.interfacePkg,
		"ipath": g.interfacePath,
		"pkg":   filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "store.gen.go"), "test/store.gg", vars, g.filters)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "test/operation.gg", vars, g.filters)
}

func (g *Generator) TransactionCache(path string) error {
	err := os.MkdirAll(path, 0o755)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg":  g.interfacePkg,
		"ipath": g.interfacePath,
		"pkg":   filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "store.gen.go"), "txcache/store.gg", vars, g.filters)
	if err != nil {
		return err
	}

	err = generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "txcache/operation.gg", vars, g.filters)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, filepath.Join(path, "select.gen.go"), "txcache/select.gg", vars, g.filters)
}

func (g *Generator) Serializer(path string) error {
	err := os.MkdirAll(path, 0o755)
	if err != nil {
		return err
	}

	vars := map[string]interface{}{
		"ipkg":  g.interfacePkg,
		"ipath": g.interfacePath,
		"pkg":   filepath.Base(path),
	}
	err = generate.Generate(g.ast, filepath.Join(path, "store.gen.go"), "serializer/store.gg", vars, g.filters)
	if err != nil {
		return err
	}

	err = generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), "serializer/operation.gg", vars, g.filters)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, filepath.Join(path, "select.gen.go"), "serializer/select.gg", vars, g.filters)
}

func (g *Generator) implementation(path string, dialect string, filterFunc func(string, string, map[string]string) func(string) (string, error), options ...Option) error {
	err := os.MkdirAll(path, 0o755)
	if err != nil {
		return err
	}

	conf := config{
		UniqueNamePrefix: "db_",
		NameFormat:       "as_id",
		replaceMap:       make(map[string]string),
	}

	for _, o := range options {
		conf = o.UpdateConfig(conf)
	}

	fOpt := gogen.FilterOption("sql", filterFunc(conf.NameFormat, conf.UniqueNamePrefix, conf.replaceMap))

	vars := map[string]interface{}{
		"ipkg":   g.interfacePkg,
		"ipath":  g.interfacePath,
		"pkg":    filepath.Base(path),
		"config": conf,
	}

	opts := append([]gogen.Option{}, fOpt)
	opts = append(opts, g.filters...)

	err = generate.Generate(g.ast, filepath.Join(path, dialect+".gen.go"), dialect+"/"+dialect+".gg", vars, opts)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "operation.gen.go"), dialect+"/operation.gg", vars, opts)
	if err != nil {
		return err
	}
	err = generate.Generate(g.ast, filepath.Join(path, "create.gen.sql"), dialect+"/create.gg", vars, opts)
	if err != nil {
		return err
	}
	return generate.Generate(g.ast, filepath.Join(path, "drop.gen.sql"), dialect+"/drop.gg", vars, opts)
}

func (g *Generator) Postgres(path string, options ...Option) error {
	return g.implementation(path, "postgres", generate.PostgresName, options...)
}

func (g *Generator) MySQL(path string, options ...Option) error {
	return g.implementation(path, "mysql", generate.MySQLName, options...)
}

func (g *Generator) SQLite(path string, options ...Option) error {
	return g.implementation(path, "sqlite", generate.SQLiteName, options...)
}

func (g *Generator) MSSQL(path string, options ...Option) error {
	return g.implementation(path, "mssql", generate.MSSQLName, options...)
}

func (g *Generator) PlantUML(path string, insertTop, insertBottom string) error {
	err := os.MkdirAll(path, 0o755)
	if err != nil {
		return err
	}

	return generate.Generate(g.ast, filepath.Join(path, "store.gen.pu"), "plantuml/plantuml.gg", map[string]interface{}{
		"insertTop":    insertTop,
		"insertBottom": insertBottom,
	}, g.filters)
}
