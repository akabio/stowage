package stowage

import (
	"go/ast"
	"go/token"

	"golang.org/x/tools/go/packages"
)

// interfaceExists TODO slow!
func interfaceExists(pkgPath, interfaceName string) (bool, error) {
	cfg := &packages.Config{
		Dir:  pkgPath,
		Mode: packages.NeedCompiledGoFiles | packages.NeedFiles | packages.NeedSyntax,
	}
	pkgs, err := packages.Load(cfg, "./...")
	if err != nil {
		return false, err
	}

	for _, pkg := range pkgs {
		for _, file := range pkg.Syntax {
			for _, decl := range file.Decls {
				if genDecl, ok := decl.(*ast.GenDecl); ok && genDecl.Tok == token.TYPE {
					for _, spec := range genDecl.Specs {
						if typeSpec, ok := spec.(*ast.TypeSpec); ok {
							if _, ok := typeSpec.Type.(*ast.InterfaceType); ok {
								if typeSpec.Name.Name == interfaceName {
									return true, nil
								}
							}
						}
					}
				}
			}
		}
	}
	return false, nil
}
