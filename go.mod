module gitlab.com/akabio/stowage

go 1.18

require (
	github.com/akabio/expect v0.10.2
	github.com/antlr4-go/antlr/v4 v4.13.0
	github.com/denisenkom/go-mssqldb v0.12.3
	github.com/ghodss/yaml v1.0.0
	github.com/go-sql-driver/mysql v1.7.1
	github.com/hashicorp/golang-lru v0.5.4
	github.com/lib/pq v1.10.9
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	gitlab.com/akabio/expect v0.9.9
	gitlab.com/akabio/fmtid v0.2.2
	gitlab.com/akabio/gogen v0.6.1
	gitlab.com/akabio/gopath v0.2.0
	gitlab.com/akabio/iotool v0.5.4
	gitlab.com/akabio/mksql v0.5.1
	golang.org/x/tools v0.9.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe // indirect
	github.com/golang-sql/sqlexp v0.1.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/exp v0.0.0-20230801115018-d63ba01acd4b // indirect
	golang.org/x/mod v0.11.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
